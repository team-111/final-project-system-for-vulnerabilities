import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity('banstatus')
export class BanStatus {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ type: 'boolean', default: false })
  isBanned: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  createdOn: Date;

  @Column({ type: 'date', default: null })
  expirationDate: Date;

  @Column({ type: 'nvarchar', default: null })
  banReason: string;
}
