import { UserRole } from './../../models/enums/user-roles.enum';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { BanStatus } from './banstatus.entity';
import { Inventory } from './inventory.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true })
  username: string;

  @Column({ type: 'nvarchar', nullable: false })
  password: string;

  @Column({ type: 'nvarchar', nullable: false })
  firstName: string;

  @Column({ type: 'nvarchar', nullable: false })
  lastName: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true })
  email: string;

  @Column({ type: 'nvarchar', default: null })
  avatarUrl: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdOn: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedOn: Date;

  @DeleteDateColumn({ type: 'timestamp' })
  deletedOn: Date;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @Column({
    type: 'enum',
    enum: UserRole,
    nullable: false,
    default: UserRole.Basic,
  })
  userRole: string;

  @OneToOne(type => BanStatus, { eager: true })
  @JoinColumn()
  banStatus: BanStatus;

  @OneToMany(
    type => Inventory,
    inventory => inventory.user,
  )
  products: Promise<Inventory[]>;
}
