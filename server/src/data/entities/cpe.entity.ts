import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, Index, Unique } from 'typeorm';
import { Vendor } from './vendors.entity';
import { Name } from './names.entity';

@Entity('cpes')
export class CPE {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'nvarchar', nullable: false,  })
  title: string;

  @Column({ type: 'nvarchar', nullable: false,  })
  cpeUri23: string;

  @Column({ type: 'nvarchar', nullable: false,  })
  version: string;

  @ManyToOne(
    type => Name,
    name => name.cpes,
  )
  name: Name;

  @ManyToOne(
    type => Vendor,
    vendor => vendor.cpes,
  )
  vendor: Vendor;

}
