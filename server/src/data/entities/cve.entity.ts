import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToMany,
  JoinTable,
  UpdateDateColumn,
} from 'typeorm';
import { Inventory } from './inventory.entity';

@Entity('cves')
export class CVE {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'nvarchar', nullable: false })
  cveId: string;

  @Column({ type: 'nvarchar', nullable: false })
  severity: string;

  @Column({ type: 'boolean', default: false})
  vulnerable: boolean;

  @Column({ type: 'text', nullable: false })
  description: string;

  @UpdateDateColumn({ type: 'timestamp' })
  lastModifiedDate: Date;

  @CreateDateColumn({ type: 'timestamp' })
  publishedDate: Date;

  @Column({ type: 'nvarchar', nullable: false })
  matchedUri: string;

  @ManyToMany(
    type => Inventory,
    inventory => inventory.cves,
  )
  @JoinTable({
    name: 'product_cves',
  })
  inventory: Promise<Inventory[]>;
}
