import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { CPE } from './cpe.entity';

@Entity('names')
export class Name {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'nvarchar', nullable: false })
  name: string;

  @OneToMany(
    type => CPE,
    cpes => cpes.name,
  )
  cpes: Promise<CPE[]>;
}
