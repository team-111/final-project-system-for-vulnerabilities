import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  OneToOne,
  JoinColumn,
  ManyToMany,
  ManyToOne,
} from 'typeorm';
import { CPE } from './cpe.entity';
import { CVE } from './cve.entity';
import { User } from './user.entity';

@Entity('inventory')
export class Inventory {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'nvarchar', nullable: false})
  productName: string;

  @Column({ type: 'nvarchar', nullable: false })
  productVendor: string;

  @Column({ type: 'nvarchar', nullable: false })
  productVersion: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdOn: Date;

  @Column({ type: 'boolean', default: false })
  hasCVE: boolean;

  @Column({ type: 'boolean', default: false })
  hasCPE: boolean;

  @OneToOne(type => CPE)
  @JoinColumn()
  cpes: Promise<CPE>;

  @ManyToMany(
    type => CVE,
    cves => cves.inventory,
  )
  cves: Promise<CVE[]>;

  @ManyToOne(type => User, user => user.products)
  user: Promise<User>;
}
