import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { CPE } from './cpe.entity';

@Entity('vendors')
export class Vendor {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'nvarchar', nullable: false })
  vendorName: string;

  @OneToMany(
    type => CPE,
    cpes => cpes.vendor,
  )
  cpes: Promise<CPE[]>;
}
