import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1591729986106 implements MigrationInterface {
    name = 'Initial1591729986106'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `banstatus` (`id` varchar(36) NOT NULL, `isBanned` tinyint NOT NULL DEFAULT 0, `createdOn` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `expirationDate` date NULL DEFAULT NULL, `banReason` varchar(255) NULL DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `vendors` (`id` varchar(36) NOT NULL, `vendorName` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `names` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `cpes` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `cpeUri23` varchar(255) NOT NULL, `version` varchar(255) NOT NULL, `nameId` varchar(36) NULL, `vendorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `avatarUrl` varchar(255) NULL DEFAULT NULL, `createdOn` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `deletedOn` timestamp(6) NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `userRole` enum ('Admin', 'Basic') NOT NULL DEFAULT 'Basic', `banStatusId` varchar(36) NULL, UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), UNIQUE INDEX `REL_73284a017a1b03deabe9ed279e` (`banStatusId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `inventory` (`id` varchar(36) NOT NULL, `productName` varchar(255) NOT NULL, `productVendor` varchar(255) NOT NULL, `productVersion` varchar(255) NOT NULL, `createdOn` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `hasCVE` tinyint NOT NULL DEFAULT 0, `hasCPE` tinyint NOT NULL DEFAULT 0, `cpesId` varchar(36) NULL, `userId` varchar(36) NULL, UNIQUE INDEX `REL_0ca8ab284f4b644da68085905e` (`cpesId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `cves` (`id` varchar(36) NOT NULL, `cveId` varchar(255) NOT NULL, `severity` varchar(255) NOT NULL, `vulnerable` tinyint NOT NULL DEFAULT 0, `description` text NOT NULL, `lastModifiedDate` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `publishedDate` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `matchedUri` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `product_cves` (`cvesId` varchar(36) NOT NULL, `inventoryId` varchar(36) NOT NULL, INDEX `IDX_b1d0b957086e3d163763af98b4` (`cvesId`), INDEX `IDX_de349ea6ee17ac97428915bb9b` (`inventoryId`), PRIMARY KEY (`cvesId`, `inventoryId`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `cpes` ADD CONSTRAINT `FK_c5a5839a63667450937350787a1` FOREIGN KEY (`nameId`) REFERENCES `names`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `cpes` ADD CONSTRAINT `FK_d7807d5978eb1550067fa454705` FOREIGN KEY (`vendorId`) REFERENCES `vendors`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_73284a017a1b03deabe9ed279e1` FOREIGN KEY (`banStatusId`) REFERENCES `banstatus`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `inventory` ADD CONSTRAINT `FK_0ca8ab284f4b644da68085905e9` FOREIGN KEY (`cpesId`) REFERENCES `cpes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `inventory` ADD CONSTRAINT `FK_fe4917e809e078929fe517ab762` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `product_cves` ADD CONSTRAINT `FK_b1d0b957086e3d163763af98b4d` FOREIGN KEY (`cvesId`) REFERENCES `cves`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `product_cves` ADD CONSTRAINT `FK_de349ea6ee17ac97428915bb9b8` FOREIGN KEY (`inventoryId`) REFERENCES `inventory`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product_cves` DROP FOREIGN KEY `FK_de349ea6ee17ac97428915bb9b8`", undefined);
        await queryRunner.query("ALTER TABLE `product_cves` DROP FOREIGN KEY `FK_b1d0b957086e3d163763af98b4d`", undefined);
        await queryRunner.query("ALTER TABLE `inventory` DROP FOREIGN KEY `FK_fe4917e809e078929fe517ab762`", undefined);
        await queryRunner.query("ALTER TABLE `inventory` DROP FOREIGN KEY `FK_0ca8ab284f4b644da68085905e9`", undefined);
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_73284a017a1b03deabe9ed279e1`", undefined);
        await queryRunner.query("ALTER TABLE `cpes` DROP FOREIGN KEY `FK_d7807d5978eb1550067fa454705`", undefined);
        await queryRunner.query("ALTER TABLE `cpes` DROP FOREIGN KEY `FK_c5a5839a63667450937350787a1`", undefined);
        await queryRunner.query("DROP INDEX `IDX_de349ea6ee17ac97428915bb9b` ON `product_cves`", undefined);
        await queryRunner.query("DROP INDEX `IDX_b1d0b957086e3d163763af98b4` ON `product_cves`", undefined);
        await queryRunner.query("DROP TABLE `product_cves`", undefined);
        await queryRunner.query("DROP TABLE `cves`", undefined);
        await queryRunner.query("DROP INDEX `REL_0ca8ab284f4b644da68085905e` ON `inventory`", undefined);
        await queryRunner.query("DROP TABLE `inventory`", undefined);
        await queryRunner.query("DROP INDEX `REL_73284a017a1b03deabe9ed279e` ON `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`", undefined);
        await queryRunner.query("DROP TABLE `users`", undefined);
        await queryRunner.query("DROP TABLE `cpes`", undefined);
        await queryRunner.query("DROP TABLE `names`", undefined);
        await queryRunner.query("DROP TABLE `vendors`", undefined);
        await queryRunner.query("DROP TABLE `banstatus`", undefined);
    }

}
