import { Repository, createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../entities/user.entity';
import { BanStatus } from '../entities/banstatus.entity';
import { UserRole } from '../../models/enums/user-roles.enum';


const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const banStatusRepo: Repository<BanStatus> = connection.manager.getRepository(
    BanStatus,
  );

  const admin = await userRepo.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const username = 'admin';
  const password = 'Aaa123';
  const hashedPassword = await bcrypt.hash(password, 10);
  const userBanStatus = banStatusRepo.create();
  const savedBanStatus = await banStatusRepo.save(userBanStatus);

  const newAdmin: User = userRepo.create({
    username,
    password: hashedPassword,
    firstName: 'Admin',
    lastName: 'Admin',
    email: 'admin@admin.bg',
    userRole: UserRole.Admin,
    banStatus: savedBanStatus,
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
