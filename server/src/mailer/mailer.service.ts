import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { InjectRepository } from '@nestjs/typeorm';
import { CVE } from 'src/data/entities/cve.entity';
import { Repository } from 'typeorm';
import { CveReturnDTO } from 'src/models/cves/cve-return.dto';
import { CPE } from 'src/data/entities/cpe.entity';
import { User } from 'src/data/entities/user.entity';
import { Inventory } from 'src/data/entities/inventory.entity';

@Injectable()
export class EmailerService {
  constructor(
    private readonly mailerService: MailerService,
    @InjectRepository(CVE) private readonly cvesRepository: Repository<CVE>,
    @InjectRepository(CPE) private readonly cpesRepository: Repository<CPE>,
    @InjectRepository(Inventory) private readonly inventoryRepository: Repository<Inventory>,
  ) { }

  public async emailCves(assignedCVE: CVE, product: Inventory, userFromReq: User): Promise<void> {
    const date = new Date();
    const foundCVe = await this.cvesRepository.findOne({
      where: {
        id: assignedCVE.id
      }
    });
    const foundProduct = await this.inventoryRepository.findOne({
      where: {
        id: product.id
      }
    });


    this
      .mailerService
      .sendMail({
        to: ['georgy_90@abv.bg', 'bonev09@abv.bg'], // list of receivers
        from: 'no.reply.team11.gp@gmail.com', // sender address
        subject: 'CVE assign ✔', // Subject line
        html: `CVE with ID- <b>'${foundCVe.cveId}'</b> was assigned to product <b>'${foundProduct.productName}- ${foundProduct.productVendor}- ${foundProduct.productVersion} '</b> from <b>'${userFromReq.username}'</b> on <b>${date}</b>`, // HTML body content

      })
      .then(() => { })
      .catch(() => { });
  }

  public async emailCpes(assignedCPE: CPE, product: Inventory, userFromReq: User): Promise<void> {
    const date = new Date();
    const foundCPE = await this.cpesRepository.findOne({
      where: {
        id: assignedCPE.id
      }
    });
    const foundProduct = await this.inventoryRepository.findOne({
      where: {
        id: product.id
      }
    });


    this
      .mailerService
      .sendMail({
        to: ['georgy_90@abv.bg', 'bonev09@abv.bg'], // list of receivers
        from: 'no.reply.team11.gp@gmail.com', // sender address
        subject: 'CPE assign ✔', // Subject line
        html: `CPE with title- <b>'${foundCPE.title}'</b> was assigned to product <b>'${foundProduct.productName}- ${foundProduct.productVendor}- ${foundProduct.productVersion} '</b> from <b>'${userFromReq.username}'</b> on <b>${date}</b>
        `, // HTML body content

      })
      .then(() => { })
      .catch(() => { });
   }

}