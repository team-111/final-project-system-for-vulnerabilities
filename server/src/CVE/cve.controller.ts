import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Param,
  Body,
  UseGuards,
  Query,
  Delete,
} from '@nestjs/common';
import { CveService } from './cve.service';
import { PopulateCveDTO } from '../models/cves/cve-populate.dto';
import { InventoryReturnDTO } from '../models/inventory/invetory-return.dto';
import { CveReturnDTO } from '../models/cves/cve-return.dto';
import { CVE } from '../data/entities/cve.entity';
import { AuthGuardWithBlacklisting } from 'src/common/guards/blacklist.guard';
import { BanGuard } from 'src/common/guards/banned.guard';
import { SearchAllCVEsDTO } from 'src/models/cves/cve-search-all.dto';
import { GetUser } from 'src/common/decorators/user.decorator';
import { User } from 'src/data/entities/user.entity';

@UseGuards(AuthGuardWithBlacklisting, BanGuard)
@Controller('cves')
export class CveController {
  constructor(private readonly cveService: CveService) {}

  @Get(':cveId')
  @HttpCode(HttpStatus.OK)
  public async searchSpecificCVE(
    @Param('cveId') cveId: string,
  ): Promise<Partial<CveReturnDTO>> {
    return await this.cveService.searchSpecificCVE(cveId);
  }

  @Get('cpes/:cpeId')
  @HttpCode(HttpStatus.OK)
  public async getCvesForCpe(@Param('cpeId') cpeId: string): Promise<CVE[]> {
    return await this.cveService.getCvesForCpe(cpeId);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  public async searchAllCVEs(
    @Query() searchAllQuery: SearchAllCVEsDTO,
  ): Promise<CveReturnDTO[] | CveReturnDTO> {
    return await this.cveService.searchAllCVEs(searchAllQuery);
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  public async populateCVE(
    @Body() matched: PopulateCveDTO,
  ): Promise<CveReturnDTO[]> {
    return await this.cveService.populateCVE(matched);
  }

  @Post(':cveId/products/:productId')
  @HttpCode(HttpStatus.CREATED)
  public async assignCveToProduct(
    @Param('cveId') cveId: string,
    @Param('productId') productId: string,
    @GetUser() userFromReq: User,
  ): Promise<InventoryReturnDTO> {
    return await this.cveService.assignCveToProduct(
      cveId,
      productId,
      userFromReq,
    );
  }

  @Delete(':cveId/product/:productId')
  @HttpCode(HttpStatus.OK)
  public async unAssignCve(
    @Param('cveId') cveId: string,
    @Param('productId') productId: string,
  ): Promise<CveReturnDTO> {
    return await this.cveService.unAssignCVE(cveId, productId);
  }
}
