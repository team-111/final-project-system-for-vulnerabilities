import { Module } from '@nestjs/common';
import { CveController } from './cve.controller';
import { CveService } from './cve.service';
import { CVE } from '../data/entities/cve.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CPE } from '../data/entities/cpe.entity';
import { Inventory } from '../data/entities/inventory.entity';
import { EmailerService } from 'src/mailer/mailer.service';

@Module({
  imports: [TypeOrmModule.forFeature([CVE, CPE, Inventory])],
  controllers: [CveController],
  providers: [CveService, EmailerService]
})
export class CveModule { }
