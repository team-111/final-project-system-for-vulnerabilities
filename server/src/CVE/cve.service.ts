import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Inventory } from 'src/data/entities/inventory.entity';
import { CVE } from 'src/data/entities/cve.entity';
import { PopulateCveDTO } from 'src/models/cves/cve-populate.dto';
import { SystemVulnerabilitiesError } from 'src/common/exceptions/system-vulnerabilities.error';
import { CveReturnDTO } from 'src/models/cves/cve-return.dto';
import { InventoryReturnDTO } from 'src/models/inventory/invetory-return.dto';
import { SearchAllCVEsDTO } from 'src/models/cves/cve-search-all.dto';
import { CPE } from 'src/data/entities/cpe.entity';
import { EmailerService } from 'src/mailer/mailer.service';
import { User } from 'src/data/entities/user.entity';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

@Injectable()
export class CveService {
  constructor(
    @InjectRepository(CVE) private readonly cveRepository: Repository<CVE>,
    @InjectRepository(CPE) private readonly cpeRepository: Repository<CPE>,
    @InjectRepository(Inventory)
    private readonly inventoryRepository: Repository<Inventory>,
    private readonly mailerService: EmailerService,
  ) {}

  public async populateCVE(matched: PopulateCveDTO): Promise<CveReturnDTO[]> {
    const replaceSlashes = matched.matchedUri.replace(/\\\\/g, '\\');
    const request = await fetch(
      `https://services.nvd.nist.gov/rest/json/cves/1.0?cpeMatchString=${replaceSlashes}`,
    );
    const response = await request.json();

    if (!response.result) {
      throw new SystemVulnerabilitiesError(
        `Can not find CVEs for CpeUri23 '${replaceSlashes}'`,
        404,
      );
    }

    try {
      return await Promise.all(
        response.result.CVE_Items.map(async el => {
          const newCve: CVE = await this.cveRepository.create();
          newCve.cveId = el.cve.CVE_data_meta.ID;
          newCve.description = el.cve.description.description_data.map(
            el => el.value,
          );
          newCve.lastModifiedDate = el.lastModifiedDate;
          newCve.severity = el.impact.baseMetricV2.severity;
          newCve.vulnerable =
            el.configurations.nodes[0].cpe_match[0].vulnerable;
          newCve.publishedDate = el.publishedDate;
          newCve.matchedUri = replaceSlashes;
          return await this.cveRepository.save(newCve);
        }),
      );
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new SystemVulnerabilitiesError(
          'Cves are already saved in the database',
          403,
        );
      }
    }
  }

  public async getCvesForCpe(cpeId: string): Promise<any> {
    const foundCpe: CPE = await this.cpeRepository.findOne({
      where: {
        id: cpeId,
      },
    });
    const foundCves = await this.cveRepository.find({
      where: {
        matchedUri: foundCpe.cpeUri23,
      },
    });
    const filteredCves = foundCves.reduce((acc, current) => {
      const x = acc.find(item => item.cveId === current.cveId);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);

    return filteredCves;
  }

  public async assignCveToProduct(
    cveId: string,
    productId: string,
    userFromReq: User,
  ): Promise<InventoryReturnDTO> {
    const foundProduct: Inventory = await this.inventoryRepository.findOne({
      where: {
        id: productId,
        hasCPE: true,
      },
      relations: ['cves'],
    });

    if (!foundProduct) {
      throw new SystemVulnerabilitiesError(
        'No such product to assign a CVE found',
        404,
      );
    }

    const foundCve: CVE = await this.cveRepository.findOne({
      where: {
        id: cveId,
      },
      relations: ['inventory'],
    });

    if (!foundCve) {
      throw new SystemVulnerabilitiesError(
        'No such CVE found to assign to a product',
        404,
      );
    }
    const foundProductCves = await foundProduct.cves;
    if (foundProductCves.some(el => el.id === foundCve.id)) {
      throw new SystemVulnerabilitiesError(
        'This cve is already assigned to this product',
        403,
      );
    }

    foundProduct.cves = Promise.resolve([...foundProductCves, foundCve]);
    foundProduct.hasCVE = true;

    this.mailerService.emailCves(foundCve, foundProduct, userFromReq);

    try {
      const savedProduct: Inventory = await this.inventoryRepository.save(
        foundProduct,
      );
      return this.toInventoryReturn(savedProduct);
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new SystemVulnerabilitiesError(
          'CVE is already is assigned to this product',
          403,
        );
      }
    }
  }

  public async unAssignCVE(
    cveId: string,
    productId: string,
  ): Promise<CveReturnDTO> {
    const product = await this.inventoryRepository.findOne(productId);
    const productCVEs = await product.cves;
    const foundCve = await this.cveRepository.findOne(cveId);

    if (productCVEs.length === 1) {
      product.hasCVE = false;
    }

    await this.inventoryRepository.save(product);
    await this.cveRepository
      .createQueryBuilder()
      .relation('inventory')
      .of(cveId)
      .remove(productId);

    return foundCve;
  }

  public async searchSpecificCVE(
    cveId: string,
  ): Promise<Partial<CveReturnDTO>> {
    const foundCVE = await this.cveRepository.findOne({
      where: {
        cveId,
      },
    });

    if (foundCVE) {
      return {
        id: foundCVE.id,
        cveId: foundCVE.cveId,
        description: foundCVE.description,
        severity: foundCVE.severity,
        vulnerable: foundCVE.vulnerable,
        matchedUri: foundCVE.matchedUri,
        lastModifiedDate: foundCVE.lastModifiedDate,
        publishedDate: foundCVE.publishedDate,
      };
    } else {
      const request = await fetch(
        `https://services.nvd.nist.gov/rest/json/cve/1.0/${cveId}`,
      );
      const response = await request.json();

      if (!response.result) {
        throw new SystemVulnerabilitiesError(
          `Can not find CVE with id '${cveId}'`,
          404,
        );
      }

      const res = response.result.CVE_Items[0];

      return {
        cveId: res.cve.CVE_data_meta.ID,
        description: res.cve.description.description_data.map(el => el.value),
        severity: res.impact.baseMetricV2.severity,
        vulnerable: res.configurations.nodes[0].cpe_match[0].vulnerable,
        matchedUri: res.configurations.nodes[0].cpe_match[0].cpe23Uri,
        lastModifiedDate: res.lastModifiedDate,
        publishedDate: res.publishedDate,
      };
    }
  }

  public async searchAllCVEs(
    searchQuery: SearchAllCVEsDTO,
  ): Promise<CveReturnDTO[] | CveReturnDTO> {
    const request = await fetch(
      `https://services.nvd.nist.gov/rest/json/cves/1.0?startIndex=${searchQuery.startIndex}&resultsPerPage=${searchQuery.resultsPerPage}`,
    );
    const response = await request.json();
    if (!response.result) {
      throw new SystemVulnerabilitiesError(`Can not find CVEs`, 404);
    }
    const res = response.result.CVE_Items.map(el => {
      return {
        cveId: el.cve.CVE_data_meta.ID,
        description: el.cve.description.description_data.map(el => el.value),
        severity: el.impact?.baseMetricV2.severity,
        vulnerable: el.configurations?.nodes.map(
          el => el.cpe_match[0].vulnerable,
        ),
        cpiUri23: el.configurations?.nodes.map(el =>
          el.cpe_match.map(el => el.cpe23Uri),
        ),
        lastModifiedDate: el.lastModifiedDate,
        publishedDate: el.publishedDate,
      };
    });

    return res;
  }

  private cveReturn(cve: CVE): CveReturnDTO {
    const value = {
      id: cve.id,
      cveId: cve.cveId,
      description: cve.description,
      severity: cve.severity,
      vulnerable: cve.vulnerable,
      matchedUri: cve.matchedUri,
      lastModifiedDate: cve.lastModifiedDate,
      publishedDate: cve.publishedDate,
    };

    return value;
  }

  private stripUnderscoreProps<T, O extends T = T>(value: O): T {
    // remove all the keys starting with __ (for lazy relations, loaded with the "relations" property)
    Object.keys(value).forEach((key: string) => {
      if (key.startsWith('__')) {
        delete value[key];
      }
    });

    return value as T;
  }

  private toInventoryReturn(inventory: Inventory): InventoryReturnDTO {
    const value = {
      ...inventory,
      // check if the lazy relations were loaded and return them or return null
      cves:
        ((inventory as any).__cves__ &&
          (inventory as any).__cves__.map(x => this.cveReturn(x))) ||
        null,
    };

    return this.stripUnderscoreProps<InventoryReturnDTO>(value);
  }
}
