import { SearchProductsByNameOrVendorDTO } from '../models/inventory/product-search-name-vendor.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Inventory } from '../data/entities/inventory.entity';
import { Repository, Like } from 'typeorm';
import { SystemVulnerabilitiesError } from '../common/exceptions/system-vulnerabilities.error';
import { AddProductDTO } from '../models/inventory/product-add.dto';
import { InventoryReturnDTO } from '../models/inventory/invetory-return.dto';
import { CVE } from '../data/entities/cve.entity';
import { CPE } from '../data/entities/cpe.entity';
import { SearchCpeByTitleDTO } from '../models/inventory/inventory-search-cpe-title.dto';
import { CveReturnDTO } from 'src/models/cves/cve-return.dto';
import { User } from 'src/data/entities/user.entity';
import { UserRole } from 'src/models/enums/user-roles.enum';
import { HasCveDTO } from 'src/models/inventory/inventory-hasCve.dto';
import { SearchProductByCveIdDTO } from 'src/models/inventory/inventory-search-cve-id.dto';
import { HasCpeDTO } from 'src/models/inventory/inventory-hasCpe.dto';
import { CpeReturnDTO } from 'src/models/cpes/cpes-return.dto';
import { UserReturnDTO } from 'src/models/users/user-return.dto';

@Injectable()
export class InventoryService {
  constructor(
    @InjectRepository(Inventory)
    private readonly inventoryRepository: Repository<Inventory>,
    @InjectRepository(CVE)
    private readonly cveRepository: Repository<CVE>,
    @InjectRepository(CPE)
    private readonly cpeRepository: Repository<CPE>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  public async getAllProducts(): Promise<InventoryReturnDTO[]> {
    const allProducts: Inventory[] = await this.inventoryRepository.find({
      select: [
        'id',
        'productName',
        'productVendor',
        'productVersion',
        'createdOn',
        'hasCVE',
        'hasCPE',
        'cpes',
      ],
      relations: ['cpes', 'user'],
    });

    return allProducts.map(el => this.toInventoryReturn(el));
  }

  public async getAllProductsCount(): Promise<number> {
    return await this.inventoryRepository.count();
  }

  public async getAllProductsForUser(
    userId: string,
  ): Promise<InventoryReturnDTO[]> {
    const foundUser = await this.usersRepository.findOne({
      where: {
        id: userId,
      },
    });
    if (!foundUser) {
      throw new SystemVulnerabilitiesError(
        `User with id ${userId} not found!`,
        404,
      );
    }

    const allProductsForUser = await this.inventoryRepository.find({
      where: {
        user: userId,
      },
    });

    return allProductsForUser;
  }

  public async getAllProductsCountForUser(userId: string) {
    const foundUser = await this.usersRepository.findOne({
      where: {
        id: userId,
      },
    });
    if (!foundUser) {
      throw new SystemVulnerabilitiesError(
        `User with id ${userId} not found!`,
        404,
      );
    }

    const allProductsCountForUser = await this.inventoryRepository.count({
      where: {
        user: userId,
      },
    });

    return allProductsCountForUser;
  }

  public async getProductsWithOrWithoutCVEs(
    hasCve: HasCveDTO,
  ): Promise<InventoryReturnDTO[]> {
    const allProducts: Inventory[] = await this.inventoryRepository.find({
      select: [
        'id',
        'productName',
        'productVendor',
        'productVersion',
        'createdOn',
        'hasCVE',
        'hasCPE',
      ],
      where: {
        hasCVE: hasCve.hasCve,
      },
      relations: ['user']
    });

    return allProducts.map(el => this.toInventoryReturn(el));
  }

  public async getSingleProduct(
    productId: string,
  ): Promise<InventoryReturnDTO> {
    const singleProduct: Inventory = await this.inventoryRepository.findOne({
      select: [
        'id',
        'productName',
        'productVendor',
        'productVersion',
        'createdOn',
        'hasCVE',
        'hasCPE',
        'cpes',
        'user',
      ],
      where: {
        id: productId,
      },
      relations: ['cpes', 'cves', 'user'],
    });

    if (!singleProduct) {
      throw new SystemVulnerabilitiesError(
        `No product with '${productId}' exists`,
        404,
      );
    }

    return this.toInventoryReturn(singleProduct);
  }

  public async countProductsWithOrWithoutCpes(
    hasCpe: HasCpeDTO,
  ): Promise<number> {
    return await this.inventoryRepository.count({
      where: {
        hasCPE: hasCpe.hasCpe,
      },
    });
  }

  public async countProductsWithOrWithoutCves(
    hasCve: HasCveDTO,
  ): Promise<number> {
    return await this.inventoryRepository.count({
      where: {
        hasCVE: hasCve.hasCve,
      },
    });
  }

  public async addProduct(
    product: AddProductDTO,
    userFromReq: User,
  ): Promise<InventoryReturnDTO> {
    const inventoryProduct: Inventory = await this.inventoryRepository.findOne({
      where: {
        productName: product.productName,
        productVendor: product.productVendor,
        productVersion: product.productVersion,
      },
    });

    if (inventoryProduct) {
      throw new SystemVulnerabilitiesError(
        `Product with '${inventoryProduct.id}' already exists`,
        403,
      );
    }
    const newInventoryProduct: Inventory = await this.inventoryRepository.create(
      product,
    );
    newInventoryProduct.cpes = Promise.resolve(null);
    newInventoryProduct.cves = Promise.resolve([]);
    newInventoryProduct.user = Promise.resolve(userFromReq);

    const savedInventoryProduct: Inventory = await this.inventoryRepository.save(
      newInventoryProduct,
    );

    return {
      id: savedInventoryProduct.id,
      productName: savedInventoryProduct.productName,
      productVendor: savedInventoryProduct.productVendor,
      productVersion: savedInventoryProduct.productVersion,
      createdOn: savedInventoryProduct.createdOn,
      hasCVE: savedInventoryProduct.hasCVE,
      hasCPE: savedInventoryProduct.hasCPE,
    };
  }

  public async deleteProduct(
    productId: string,
    userFromReq: User,
  ): Promise<void> {
    const foundProduct: Inventory = await this.inventoryRepository.findOne({
      where: {
        id: productId,
      },
    });

    const productUser = await foundProduct.user;

    if (userFromReq.userRole !== UserRole.Admin) {
      if (productUser.id !== userFromReq.id) {
        throw new SystemVulnerabilitiesError(
          'You can not delete a product unless is not yours or you are an admin',
          403,
        );
      }
    }

    await this.inventoryRepository.remove(foundProduct);
  }

  public async searchByNameOrVendor(
    querySearch: SearchProductsByNameOrVendorDTO,
  ): Promise<InventoryReturnDTO[]> {
    const { productName, productVendor } = querySearch;
    const where: any = {};
    if (productName) {
      where.productName = Like(`%${productName}%`);
    }
    if (productVendor) {
      where.productVendor = Like(`%${productVendor}%`);
    }
    const foundProducts: Inventory[] = await this.inventoryRepository.find({
      where,
      select: [
        'id',
        'productName',
        'productVendor',
        'productVersion',
        'createdOn',
        'hasCVE',
        'hasCPE',
      ],
      relations: ['cves'],
    });

    return foundProducts.map(x => this.toInventoryReturn(x));
  }

  public async searchByCveId(
    cveId: SearchProductByCveIdDTO,
  ): Promise<InventoryReturnDTO[]> {
    const foundCve: CVE = await this.cveRepository.findOne({
      where: {
        cveId: cveId.cveId,
      },
      relations: ['inventory'],
    });

    if (!foundCve) {
      return this.inventoryRepository.find();
    }

    return foundCve.inventory;
  }

  public async searchByCpeTitle(
    cpeTitle: SearchCpeByTitleDTO,
  ): Promise<InventoryReturnDTO[]> {

    const foundCpe: CPE = await this.cpeRepository.findOne({
      where: {
        title: Like(`%${cpeTitle.title}%`),
      },
    });

    if (!foundCpe) {
      return this.inventoryRepository.find();
    }

    const foundProducts: Inventory[] = await this.inventoryRepository.find({
      where: {
        cpes: foundCpe.id,
      },
    });
    return foundProducts;
  }

  private cveReturn(cve: CVE): CveReturnDTO {
    const value = {
      id: cve.id,
      cveId: cve.cveId,
      description: cve.description,
      severity: cve.severity,
      vulnerable: cve.vulnerable,
      matchedUri: cve.matchedUri,
      lastModifiedDate: cve.lastModifiedDate,
      publishedDate: cve.publishedDate,
    };

    return value;
  }

  private cpeReturn(cpe: CPE): CpeReturnDTO {
    const value = {
      id: cpe.id,
      title: cpe.title,
      cpeUri23: cpe.cpeUri23,
      version: cpe.version,
    };

    return value;
  }

  private userReturn(user: User): UserReturnDTO {
    const value = {
      id: user.id,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      createdOn: user.createdOn,
      userRole: user.userRole,
      avatarUrl: user.avatarUrl,
    };

    return value;
  }

  private stripUnderscoreProps<T, O extends T = T>(value: O): T {
    // remove all the keys starting with __ (for lazy relations, loaded with the "relations" property)
    Object.keys(value).forEach((key: string) => {
      if (key.startsWith('__')) {
        delete value[key];
      }
    });

    return value as T;
  }

  private toInventoryReturn(inventory: Inventory): InventoryReturnDTO {
    const value = {
      ...inventory,
      // check if the lazy relations were loaded and return them or return null
      cves:
        ((inventory as any).__cves__ &&
          (inventory as any).__cves__.map(x => this.cveReturn(x))) ||
        null,
      cpes:
        ((inventory as any).__cpes__ &&
          this.cpeReturn((inventory as any).__cpes__)) ||
        null,
      user:
        ((inventory as any).__user__ &&
          this.userReturn((inventory as any).__user__)) ||
        null,
    };

    return this.stripUnderscoreProps<InventoryReturnDTO>(value);
  }
}
