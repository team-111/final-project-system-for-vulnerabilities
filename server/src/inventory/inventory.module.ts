import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Inventory } from '../data/entities/inventory.entity';
import { InventoryService } from './inventory.service';
import { InventoryController } from './inventory.controller';
import { CPE } from '../data/entities/cpe.entity';
import { CVE } from '../data/entities/cve.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Inventory, CPE, CVE])],
  providers: [InventoryService,],
  controllers: [InventoryController],
})
export class InventoryModule {}
