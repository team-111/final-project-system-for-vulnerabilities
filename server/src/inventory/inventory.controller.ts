import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Body,
  Delete,
  Query,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { InventoryService } from './inventory.service';
import { AddProductDTO } from '../models/inventory/product-add.dto';
import { SearchProductsByNameOrVendorDTO } from '../models/inventory/product-search-name-vendor.dto';
import { SearchCpeByTitleDTO } from '../models/inventory/inventory-search-cpe-title.dto';
import { InventoryReturnDTO } from '../models/inventory/invetory-return.dto';
import { User } from '../data/entities/user.entity';
import { GetUser } from '../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { BanGuard } from '../common/guards/banned.guard';
import { HasCveDTO } from '../models/inventory/inventory-hasCve.dto';
import { SearchProductByCveIdDTO } from 'src/models/inventory/inventory-search-cve-id.dto';
import { HasCpeDTO } from 'src/models/inventory/inventory-hasCpe.dto';

@UseGuards(AuthGuardWithBlacklisting, BanGuard)
@Controller('products')
export class InventoryController {
  constructor(private readonly inventoryService: InventoryService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  public async getAllProducts(
    @Query() query: SearchProductsByNameOrVendorDTO,
  ): Promise<InventoryReturnDTO | InventoryReturnDTO[]> {
    if (query.productName || query.productVendor) {
      return await this.inventoryService.searchByNameOrVendor(query);
    }

    return this.inventoryService.getAllProducts();
  }

  @Get('cves')
  @HttpCode(HttpStatus.OK)
  public async getProductsWithCVEs(
    @Query() hasCve: HasCveDTO,
  ): Promise<InventoryReturnDTO[]> {
    return await this.inventoryService.getProductsWithOrWithoutCVEs(hasCve);
  }

  @Get('count')
  @HttpCode(HttpStatus.OK)
  public async getAllProductsCount(): Promise<number> {
    return await this.inventoryService.getAllProductsCount();
  }

  @Get('cpes')
  @HttpCode(HttpStatus.OK)
  public async searchByCpeTitle(
    @Query() title: SearchCpeByTitleDTO,
  ): Promise<InventoryReturnDTO[]> {
    return await this.inventoryService.searchByCpeTitle(title);
  }
  @Get('cve')
  @HttpCode(HttpStatus.OK)
  public async searchByCveId(
    @Query() cveId: SearchProductByCveIdDTO,
  ): Promise<InventoryReturnDTO[]> {
    return await this.inventoryService.searchByCveId(cveId);
  }

  @Get('cves/count')
  @HttpCode(HttpStatus.OK)
  public async countProductsWithOrWithoutCves(
    @Query() hasCve: HasCveDTO,
  ): Promise<number> {
    return await this.inventoryService.countProductsWithOrWithoutCves(hasCve);
  }

  @Get('cpe/count')
  @HttpCode(HttpStatus.OK)
  public async countProductsWithOrWithoutCpes(
    @Query() hasCpe: HasCpeDTO,
  ): Promise<number> {
    return await this.inventoryService.countProductsWithOrWithoutCpes(hasCpe);
  }

  @Get('user/:userId')
  @HttpCode(HttpStatus.OK)
  public async getAllProductsForUser(
    @Param('userId') userId: string,
  ): Promise<InventoryReturnDTO[]> {
    return await this.inventoryService.getAllProductsForUser(userId);
  }

  @Get('user/:userId/count')
  @HttpCode(HttpStatus.OK)
  public async getAllProductsCountForUser(
    @Param('userId') userId: string,
  ): Promise<number> {
    return await this.inventoryService.getAllProductsCountForUser(userId);
  }

  @Get('cve/count')
  @HttpCode(HttpStatus.OK)
  public async countProductsWithoutCves(
    @Query() hasCve: HasCveDTO,
  ): Promise<number> {
    return await this.inventoryService.countProductsWithOrWithoutCves(hasCve);
  }

  @Get('/single/:productId')
  @HttpCode(HttpStatus.OK)
  public async getSingleProduct(
    @Param('productId') productId: string,
  ): Promise<InventoryReturnDTO> {
    return await this.inventoryService.getSingleProduct(productId);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async addInventoryProduct(
    @Body() inventoryProduct: AddProductDTO,
    @GetUser() userFromReq: User,
  ): Promise<InventoryReturnDTO> {
    return await this.inventoryService.addProduct(
      inventoryProduct,
      userFromReq,
    );
  }

  @Delete(':productId')
  @HttpCode(HttpStatus.OK)
  public async deleteProduct(
    @Param('productId') productId: string,
    @GetUser() userFromReq,
  ): Promise<void> {
    return await this.inventoryService.deleteProduct(productId, userFromReq);
  }
}
