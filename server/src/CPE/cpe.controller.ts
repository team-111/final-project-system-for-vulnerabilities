import { MatchProductDTO } from './../models/inventory/product-match.dto';
import { CPE } from './../data/entities/cpe.entity';
import {
  Controller,
  Get,
  HttpStatus,
  HttpCode,
  Post,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CpeService } from './cpe.service';
import { InventoryReturnDTO } from '../models/inventory/invetory-return.dto';
import { AuthGuardWithBlacklisting } from 'src/common/guards/blacklist.guard';
import { BanGuard } from 'src/common/guards/banned.guard';
import { GetUser } from 'src/common/decorators/user.decorator';
import { User } from 'src/data/entities/user.entity';

@Controller('cpes')
export class CpeController {
  constructor(private readonly cpeService: CpeService) {}

  @Get('products/:productId')
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async matchCpe(
    @Param('productId') productId: string,
  ): Promise<CPE | CPE[]> {
    return await this.cpeService.matchCpe(productId);
  }

  @Get('count')
  @HttpCode(HttpStatus.OK)
  public async getAllCPEsCount(): Promise<number> {
    return await this.cpeService.getAllCPEsCount();
  }

  @Get('names/count')
  @HttpCode(HttpStatus.OK)
  public async getAllCpeNamesCount(): Promise<number> {
    return await this.cpeService.getAllCpeNamesCount();
  }

  @Get('vendors/count')
  @HttpCode(HttpStatus.OK)
  public async getAllCpeVendorsCount(): Promise<number> {
    return await this.cpeService.getAllCpeVendorsCount();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async populateCpeDictionary(): Promise<void> {
    return await this.cpeService.cpeDictionary();
  }

  @Post(':cpeId/products/:productId')
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @HttpCode(HttpStatus.CREATED)
  public async assignCpeToProduct(
    @Param('cpeId') cpeId: string,
    @Param('productId') productId: string,
    @GetUser() userFromReq: User
  ): Promise<InventoryReturnDTO> {
    return await this.cpeService.assignCpeToProduct(cpeId, productId, userFromReq);
  }
}
