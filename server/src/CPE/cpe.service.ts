import { MatchProductDTO } from './../models/inventory/product-match.dto';
import { CPE } from './../data/entities/cpe.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Brackets } from 'typeorm';
import * as fs from 'fs';
import * as readline from 'readline';
import { Stream } from 'stream';
import { Name } from '../data/entities/names.entity';
import { Vendor } from '../data/entities/vendors.entity';
import { Inventory } from '../data/entities/inventory.entity';
import { SystemVulnerabilitiesError } from '../common/exceptions/system-vulnerabilities.error';
import { InventoryReturnDTO } from 'src/models/inventory/invetory-return.dto';
import { CpeReturnDTO } from 'src/models/cpes/cpes-return.dto';
import { EmailerService } from 'src/mailer/mailer.service';
import { User } from 'src/data/entities/user.entity';

@Injectable()
export class CpeService {
  constructor(
    @InjectRepository(CPE)
    private readonly cpeRepository: Repository<CPE>,
    @InjectRepository(Name)
    private readonly nameRepository: Repository<Name>,
    @InjectRepository(Vendor)
    private readonly vendorsRepository: Repository<Vendor>,
    @InjectRepository(Inventory)
    private readonly inventoryRepository: Repository<Inventory>,
    private readonly mailerService: EmailerService,
  ) { }

  public async matchCpe(productId: string): Promise<CPE | CPE[]> {
    const foundProduct: Inventory = await this.inventoryRepository.findOne({
      where: {
        id: productId,
      },
    });
    const searchName = foundProduct.productName.toLocaleLowerCase();
    const searchVendor = foundProduct.productVendor.toLocaleLowerCase();

    let fullMatch: CPE[];

    let query = this.cpeRepository
      .createQueryBuilder('cpe')
      .innerJoinAndSelect('cpe.vendor', 'vendor')
      .innerJoinAndSelect('cpe.name', 'name')
      .where(
        new Brackets(qb => {
          qb.where('name.name LIKE :nameSearch', {
            nameSearch: searchName,
          })
            .orWhere('name.name LIKE :nameSearch', {
              nameSearch: `${searchName}%`,
            })
            .orWhere('name.name LIKE :nameSearch', {
              nameSearch: `%${searchName}%`,
            });
        }),
      )
      .andWhere(
        new Brackets(qb => {
          qb.where('vendor.vendorName LIKE :vendorSearch', {
            vendorSearch: searchVendor,
          })
            .orWhere('vendor.vendorName LIKE :vendorSearch', {
              vendorSearch: `${searchVendor}%`,
            })
            .orWhere('vendor.vendorName LIKE :vendorSearch', {
              vendorSearch: `%${searchVendor}%`,
            });
        }),
      );

    fullMatch = await query
      .andWhere('cpe.version like :versionSearch', {
        versionSearch: foundProduct.productVersion,
      })
      .take(100)
      .getMany();

    if (fullMatch.length === 0) {
      fullMatch = await query
        .andWhere('cpe.version like :versionSearch', {
          versionSearch: `%`,
        })
        .take(100)
        .getMany();
    }

    return fullMatch ? fullMatch : [];
  }

  public async getAllCPEsCount(): Promise<number> {
    return await this.cpeRepository.count();
  }

  public async getAllCpeNamesCount(): Promise<number> {
    return await this.nameRepository.count();
  }

  public async getAllCpeVendorsCount(): Promise<number> {
    return await this.vendorsRepository.count();
  }

  public async assignCpeToProduct(
    cpeId: string,
    productId: string,
    userFromReq: User,
  ): Promise<InventoryReturnDTO> {
    const foundProduct: Inventory = await this.inventoryRepository.findOne({
      where: {
        id: productId,
        hasCPE: false,
      },
    });
    if (!foundProduct) {
      throw new SystemVulnerabilitiesError('No such product found', 404);
    }

    const foundCPE: CPE = await this.cpeRepository.findOne({
      where: {
        id: cpeId,
      },
    });

    if (!foundCPE) {
      throw new SystemVulnerabilitiesError(
        `No such cpe with id '${cpeId}' found`,
        404,
      );
    }

    foundProduct.cpes = Promise.resolve(foundCPE);
    foundProduct.hasCPE = true;

    this.mailerService.emailCpes(foundCPE, foundProduct, userFromReq);

    try {
      const savedProduct: Inventory = await this.inventoryRepository.save(
        foundProduct,
      );
      return this.toInventoryReturn(savedProduct);
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new SystemVulnerabilitiesError(
          'CPE is already is assigned to this product',
          403,
        );
      }
    }
  }

  public async cpeDictionary(): Promise<void> {
    const cpes: Set<any> = new Set();
    const vendors: Set<string> = new Set();
    const names: Set<any> = new Set();

    let instream = fs.createReadStream('official-cpe-dictionary_v2.3.xml');
    let outstream = new Stream();
    let rl = readline.createInterface(instream, outstream as any);
    let title = '';
    let cpeUri = '';
    let name = '';
    let vendor = '';
    let version = '';
    let outCpes = [];
    let outNames = [];
    let outVendors = [];

    //   let counter = 0
    console.log('Data seed starts')
    rl.on(`line`, async line => {
      //   counter++
      // if (counter > 1000) {
      //     instream.destroy();
      // }
      if (line.includes('<title ')) {
        title = line.substring(28, line.length - 8);
      }

      if (line.includes('<cpe-23:cpe23-item')) {
        cpeUri = line.substring(29, line.length - 3);
        const cpeArr = cpeUri.split(':');
        if (cpeArr[2] === 'a') {
          vendor = cpeArr[3].replace(/_/g, ' ');
          vendors.add(vendor);

          name = cpeArr[4].replace(/_/g, ' ');
          names.add(name);

          version = cpeArr[5];
        }
      }

      if (line.includes(`</cpe-item>`)) {
        if (cpeUri.split(':')[2] === 'a') {
          cpes.add({ title: title, cpeUri23: cpeUri, name, vendor, version });
        }
        (title = ''), (cpeUri = ''), (name = ''), (vendor = ''), (version = '');
      }
    });

    instream.on('close', async () => {
      outCpes = Array.from(cpes);
      names.forEach(item => outNames.push({ name: item }));
      vendors.forEach(item => outVendors.push({ vendorName: item }));

      const saveVendors = await this.vendorsRepository.save(outVendors, {
        chunk: 2000,
      });
      const vendorsMap = new Map<string, Vendor>(
        saveVendors.map(vendor => [vendor.vendorName, vendor]),
      );
      const saveProductNames = await this.nameRepository.save(outNames, {
        chunk: 2000,
      });
      const productNamesMap = new Map<string, Name>(
        saveProductNames.map(product => [product.name, product]),
      );

      await this.cpeRepository.save(
        outCpes.map(cpe => {
          cpe.vendor = vendorsMap.get(cpe.vendor);
          cpe.name = productNamesMap.get(cpe.name);
          return cpe;
        }),
        { chunk: 2000 },
      );
      console.log('Data seed finished');

    });
  }

  private cpeReturn(cpe: CPE): CpeReturnDTO {
    const value = {
      id: cpe.id,
      title: cpe.title,
      cpeUri23: cpe.cpeUri23,
      version: cpe.version,
    };

    return value;
  }

  private stripUnderscoreProps<T, O extends T = T>(value: O): T {
    // remove all the keys starting with __ (for lazy relations, loaded with the "relations" property)
    Object.keys(value).forEach((key: string) => {
      if (key.startsWith('__')) {
        delete value[key];
      }
    });

    return value as T;
  }

  private toInventoryReturn(inventory: Inventory): InventoryReturnDTO {
    const value = {
      ...inventory,
      // check if the lazy relations were loaded and return them or return null
      cpes:
        ((inventory as any).__cpes__ &&
          this.cpeReturn((inventory as any).__cpes__)) ||
        null,
    };

    return this.stripUnderscoreProps<InventoryReturnDTO>(value);
  }
}
