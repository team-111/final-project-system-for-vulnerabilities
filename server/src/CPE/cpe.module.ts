import { User } from 'src/data/entities/user.entity';
import { Vendor } from './../data/entities/vendors.entity';
import { Name } from './../data/entities/names.entity';
import { CPE } from './../data/entities/cpe.entity';
import { Module } from '@nestjs/common';
import { CpeService } from './cpe.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CpeController } from './cpe.controller';
import { Inventory } from '../data/entities/inventory.entity';
import { CVE } from '../data/entities/cve.entity';
import { InventoryService } from '../inventory/inventory.service';
import { InventoryController } from '../inventory/inventory.controller';
import { EmailerService } from 'src/mailer/mailer.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([CPE, Inventory, Name, CVE, Vendor, User]),
  ],
  providers: [CpeService, InventoryService, EmailerService],
  controllers: [CpeController, InventoryController],
})
export class CpeModule {}
