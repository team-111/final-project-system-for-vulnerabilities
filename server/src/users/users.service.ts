import { Injectable } from '@nestjs/common';
import { User } from '../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserReturnDTO } from '../models/users/user-return.dto';
import * as bcrypt from 'bcrypt';
import { CreateUserDTO } from '../models/users/user-create.dto';
import { UpdateUserDTO } from '../models/users/user-update.dto';
import { SystemVulnerabilitiesError } from '../common/exceptions/system-vulnerabilities.error';
import { JwtService } from '@nestjs/jwt';
import * as path from 'path';
import * as fs from 'fs';
import { UserRoleUpdateDTO } from '../models/users/user-role-update.dto';
import { UserBanDTO } from '../models/users/user-ban.dto';
import { BanStatus } from '../data/entities/banstatus.entity';
import { Cron } from '@nestjs/schedule';
import { JWTPayload } from '../auth/payload/jwt-payload';
import { Inventory } from 'src/data/entities/inventory.entity';
import { InventoryReturnDTO } from 'src/models/inventory/invetory-return.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(BanStatus)
    private banStatusRepository: Repository<BanStatus>,
    private readonly jwtService: JwtService,
  ) { }

  public async findAll(): Promise<UserReturnDTO[]> {
    const allUsers = await this.userRepository.find({
      select: [
        'id',
        'username',
        'firstName',
        'lastName',
        'email',
        'createdOn',
        'userRole',
        'avatarUrl',
      ],
      where: { isDeleted: false },
      relations: ['banStatus'],
    });

    return allUsers;
  }

  public async getAllUsersCount(): Promise<number> {
    return await this.userRepository.count();
  }

  public async findSingleUser(userId: string): Promise<UserReturnDTO> {
    const foundSingleUser: User = await this.userRepository.findOne({
      where: {
        id: userId,
        isDeleted: false,
      },
      select: [
        'id',
        'username',
        'firstName',
        'lastName',
        'email',
        'createdOn',
        'userRole',
        'avatarUrl',
      ],
      relations: ['products', 'banStatus'],
    });
    if (!foundSingleUser) {
      throw new SystemVulnerabilitiesError(
        `No user with '${userId}' exists`,
        404,
      );
    }

    return this.toUserReturnDTO(foundSingleUser);
  }

  public async createUser(user: CreateUserDTO): Promise<UserReturnDTO> {
    const foundUser: User = await this.userRepository.findOne({
      username: user.username,
      isDeleted: false,
    });
    
    if (foundUser) {
      throw new SystemVulnerabilitiesError('User already exists', 403);
    }
    const newBanStatus = await this.createBanStatus();
    const newUser: User = await this.userRepository.create(user);
    newUser.password = await bcrypt.hash(newUser.password, 10);
    newUser.banStatus = newBanStatus;
    newUser.products = Promise.resolve([]);
    
    const savedUser = await this.userRepository.save(newUser);
    return {
      id: savedUser.id,
      username: savedUser.username,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      createdOn: savedUser.createdOn,
      userRole: savedUser.userRole,
      avatarUrl: savedUser.avatarUrl,
    };
  }

  public async updateUser(
    updateUser: UpdateUserDTO,
    userFromReq: User,
  ): Promise<UserReturnDTO> {
    const keys = ['username', 'password', 'firstName', 'lastName', 'email'];

    Object.keys(updateUser).forEach((key: string) => {
      if (keys.includes(key)) {
        userFromReq[key] = updateUser[key];
      }
    });
    userFromReq.updatedOn = new Date();

    const savedUser: User = await this.userRepository.save(userFromReq);
    return {
      id: savedUser.id,
      username: savedUser.username,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      createdOn: savedUser.createdOn,
      userRole: savedUser.userRole,
      avatarUrl: savedUser.avatarUrl,
    };
  }

  public async deleteUser(userId: string): Promise<any> {
    const foundUser: User = await this.foundUserById(userId);

    foundUser.isDeleted = true;
    foundUser.deletedOn = new Date();

    const savedUser: User = await this.userRepository.save(foundUser);

    return {
      id: savedUser.id,
      username: savedUser.username,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      createdOn: savedUser.createdOn,
      userRole: savedUser.userRole,
      avatarUrl: savedUser.avatarUrl,
    };
  }

  public async changeAvatar(id: string, user: any): Promise<{ token: string }> {
    const foundUser: User = await this.userRepository.findOne({ id });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new SystemVulnerabilitiesError('No such user found', 404);
    }

    if (id === user.id) {
      throw new SystemVulnerabilitiesError(
        'You are not authorized to edit avatarUrl',
      );
    }
    foundUser.avatarUrl = user.avatarUrl;
    const savedUser: User = await this.userRepository.save(foundUser);

    const payloadUser: JWTPayload = {
      id: savedUser.id,
      username: savedUser.username,
      avatarUrl: savedUser.avatarUrl,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      createdOn: savedUser.createdOn,
      userRole: savedUser.userRole,
    };

    return {
      token: await this.jwtService.signAsync(payloadUser),
    };
  }

  public async deleteAvatarUrl(
    userId: string,
    user: any,
  ): Promise<{ token: string }> {
    const foundUser = await this.userRepository.findOne({
      where: {
        id: userId,
      },
    });
    if (!foundUser.avatarUrl) {
      throw new SystemVulnerabilitiesError(
        'You do not have an avatar yet',
        400,
      );
    }
    const filePath = path.join(__dirname, '../../avatars', foundUser.avatarUrl);
    try {
      await new Promise((resolve, reject) => {
        fs.unlink(filePath, err => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    } catch (e) {
      throw new SystemVulnerabilitiesError(e.message.message, 404);
    }
    foundUser.avatarUrl = '';
    const savedUser: User = await this.userRepository.save(foundUser);

    const payloadUser: JWTPayload = {
      id: savedUser.id,
      username: savedUser.username,
      avatarUrl: savedUser.avatarUrl,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      createdOn: savedUser.createdOn,
      userRole: savedUser.userRole,
    };

    return {
      token: await this.jwtService.signAsync(payloadUser),
    };
  }

  public async changeUserRole(
    userId: string,
    roleToChange: UserRoleUpdateDTO,
  ): Promise<UserReturnDTO> {
    const foundUser: User = await this.foundUserById(userId);
    if (foundUser.userRole === roleToChange.role) {
      throw new SystemVulnerabilitiesError(
        `This user is already "${roleToChange.role}"`,
        403,
      );
    }
    foundUser.userRole = roleToChange.role;

    const savedUser: User = await this.userRepository.save(foundUser);

    return {
      id: savedUser.id,
      username: savedUser.username,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      createdOn: savedUser.createdOn,
      userRole: savedUser.userRole,
      avatarUrl: savedUser.avatarUrl,
    };
  }

  public async banUser(userId: string, banInfo: UserBanDTO): Promise<any> {
    const foundUser: User = await this.foundUserById(userId);

    const { banReason, expirationDate } = banInfo;
    const userBanStatus = foundUser.banStatus;
    if (userBanStatus.isBanned) {
      throw new SystemVulnerabilitiesError('This user is already banned!', 403);
    }
    const createdNewDateFromExpirationDate = new Date(expirationDate);
    const curentDate = new Date();

    if (createdNewDateFromExpirationDate.getTime() < curentDate.getTime()) {
      throw new SystemVulnerabilitiesError(
        'You can not ban user with previous date!',
        403,
      );
    }

    userBanStatus.isBanned = true;
    userBanStatus.banReason = banReason;
    userBanStatus.expirationDate = createdNewDateFromExpirationDate;

    return await this.banStatusRepository.save(userBanStatus);
  }

  public async unBanUser(userId: string): Promise<any> {
    const foundUser: User = await this.foundUserById(userId);

    const userBanStatus = foundUser.banStatus;
    if (userBanStatus.isBanned) {
      userBanStatus.isBanned = false;
      userBanStatus.expirationDate = null;
      userBanStatus.banReason = null;
    } else {
      throw new SystemVulnerabilitiesError('This user is not banned', 403);
    }
    return await this.banStatusRepository.save(userBanStatus);
  }

  @Cron('0 1 00 * * 1-6')
  public async autoRemoveBan() {
    const bannedUsers = await this.banStatusRepository.find({
      where: { isBanned: true },
    });

    const currentDateTimeMilisec = new Date().getTime();

    bannedUsers.forEach(async banStatus => {
      if (
        new Date(banStatus.expirationDate).getTime() < currentDateTimeMilisec
      ) {
        banStatus.isBanned = false;
        banStatus.expirationDate = null;
        banStatus.banReason = null;
        await this.banStatusRepository.save(banStatus);
      }
    });
  }

  public async findUserByUsername(username: string) {
    const foundUser: User = await this.userRepository.findOne({
      where: {
        username,
        isDeleted: false
      },
    });
    if (!foundUser) {
      throw new SystemVulnerabilitiesError(
        'User with such username does not exist!',
        404,
      );
    }
    return foundUser;
  }

  private async foundUserById(userId: string): Promise<User> {
    const foundUser = await this.userRepository.findOne({
      id: userId,
      isDeleted: false,
    });
    if (!foundUser) {
      throw new SystemVulnerabilitiesError(
        `User with such id '${userId}' does not exists`,
        404,
      );
    }

    return foundUser;
  }

  private async createBanStatus(): Promise<BanStatus> {
    const banStatus: BanStatus = await this.banStatusRepository.create();
    const savedBanStatus = await this.banStatusRepository.save(banStatus);

    return savedBanStatus;
  }

  public async validateUserPassword(passwordToCheck: string, user: User) {
    return await bcrypt.compare(passwordToCheck, user.password);
  }

  private stripUnderscoreProps<T, O extends T = T>(value: O): T {
    // remove all the keys starting with __ (for lazy relations, loaded with the "relations" property)
    Object.keys(value).forEach((key: string) => {
      if (key.startsWith('__')) {
        delete value[key];
      }
    });

    return value as T;
  }

  private toProductsReturn(product: Inventory): InventoryReturnDTO {
    const value = {
      id: product.id,
      productName: product.productName,
      productVendor: product.productVendor,
      productVersion: product.productVersion,
      createdOn: product.createdOn,
      hasCVE: product.hasCVE,
      hasCPE: product.hasCPE,
    };
    return value;
  }

  private toUserReturnDTO(user: User): UserReturnDTO {
    const value = {
      ...user,
      products:
        ((user as any).__products__ &&
          (user as any).__products__.map(x => this.toProductsReturn(x))) ||
        null,
    };

    return this.stripUnderscoreProps<UserReturnDTO>(value);
  }
}
