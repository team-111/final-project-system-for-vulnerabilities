import { UpdateUserDTO } from 'src/models/users/user-update.dto';
import { AdminGuard } from './../common/guards/admin.guard';
import { UserReturnDTO } from 'src/models/users/user-return.dto';
import {
  Controller,
  Body,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseInterceptors,
  UploadedFile,
  Delete,
  Put,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDTO } from 'src/models/users/user-create.dto';
import { GetUser } from 'src/common/decorators/user.decorator';
import { User } from 'src/data/entities/user.entity';
import { AvatarUrlDTO } from 'src/models/users/user-avatarUrl.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserRoleUpdateDTO } from 'src/models/users/user-role-update.dto';
import { UserBanDTO } from 'src/models/users/user-ban.dto';
import { AuthGuardWithBlacklisting } from 'src/common/guards/blacklist.guard';
import { BanGuard } from 'src/common/guards/banned.guard';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @HttpCode(HttpStatus.OK)
  async findAllUsers(): Promise<UserReturnDTO[]> {
    return await this.usersService.findAll();
  }

  @Get('count')
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @HttpCode(HttpStatus.OK)
  async getAllUsersCount(): Promise<number> {
    return await this.usersService.getAllUsersCount();
  }

  @Get(':userId')
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @HttpCode(HttpStatus.OK)
  async getSingleUser(@Param('userId') userId: string): Promise<UserReturnDTO> {
    return await this.usersService.findSingleUser(userId);
  }

  @Post()
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard, BanGuard)
  @HttpCode(HttpStatus.CREATED)
  async createUser(@Body() user: CreateUserDTO): Promise<UserReturnDTO> {
    return await this.usersService.createUser(user);
  }

  @Put()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  async updateUser(
    @Body() updateUser: UpdateUserDTO,
    @GetUser() userFromReq: User,
  ): Promise<UserReturnDTO> {
    return await this.usersService.updateUser(updateUser, userFromReq);
  }

  @Delete(':userId')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  async deleteUser(@Param('userId') userId: string): Promise<UserReturnDTO> {
    return await this.usersService.deleteUser(userId);
  }

  @Post(':userId/avatar')
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @UseInterceptors(FileInterceptor('file'))
  public async uploadUserAvatar(
    @Param('userId') userId: any,
    @UploadedFile() file: any,
    @GetUser() userFromReq: User,
  ): Promise<any> {
    const updateUserProperties: AvatarUrlDTO = {
      avatarUrl: file?.filename,
    };

    return await this.usersService.changeAvatar(userId, updateUserProperties);
  }

  @Delete(':userId/avatar')
  @UseGuards(AuthGuardWithBlacklisting, BanGuard)
  @HttpCode(HttpStatus.OK)
  async deleteUserAvatar(
    @Param('userId') userId: string,
    @UploadedFile() file: any,
    @GetUser() userFromReq: User,
  ): Promise<any> {
    const updateUserProperties: AvatarUrlDTO = {
      avatarUrl: file?.filename,
    };
    return await this.usersService.deleteAvatarUrl(
      userId,
      updateUserProperties,
    );
  }

  @Put(':userId/roles')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  async updateUserRole(
    @Param('userId') userId: string,
    @Body() role: UserRoleUpdateDTO,
  ): Promise<UserReturnDTO> {
    return await this.usersService.changeUserRole(userId, role);
  }

  @Post(':userId/banstatus')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  async banUser(
    @Param('userId') userId: string,
    @Body() banInfo: UserBanDTO,
  ): Promise<any> {
    return await this.usersService.banUser(userId, banInfo);
  }

  @Put(':userId/banstatus')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  async unBanUser(@Param('userId') userId: string): Promise<any> {
    return await this.usersService.unBanUser(userId);
  }
}
