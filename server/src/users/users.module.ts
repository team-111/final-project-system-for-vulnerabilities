import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/data/entities/user.entity';
import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/data/database/database.module';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { BanStatus } from 'src/data/entities/banstatus.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { SystemVulnerabilitiesError } from 'src/common/exceptions/system-vulnerabilities.error';

@Module({
  imports: [TypeOrmModule.forFeature([User, BanStatus]), DatabaseModule,
  MulterModule.register({
    fileFilter(_, file, cb) {
      const ext = extname(file.originalname);
      const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

      if (!allowedExtensions.includes(ext)) {
        return cb(
          new SystemVulnerabilitiesError('Only images are allowed', 400),
          false,
        );
      }

      cb(null, true);
    },
    storage: diskStorage({
      destination: './avatars',
      filename: (_, file, cb) => {
        const randomName = Array.from({ length: 32 })
          .map(() => Math.round(Math.random() * 10))
          .join('');

        return cb(null, `${randomName}${extname(file.originalname)}`);
      },
    }),
  }),],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService, TypeOrmModule],
})
export class UsersModule { }
