import { Expose } from "class-transformer";

export class AvatarUrlDTO {
    @Expose()
    avatarUrl: string;
}