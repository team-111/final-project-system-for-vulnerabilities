import { IsNotEmpty, IsString, Length, IsEmail, IsOptional, MaxLength } from 'class-validator'

export class UpdateUserDTO {
    @IsNotEmpty()
    @IsString()
    @Length(3, 15)
    @IsOptional()
    username?: string;

    @IsNotEmpty()
    @IsString()
    @Length(6, 32)
    @IsOptional()
    password?: string;

    @IsNotEmpty()
    @IsString()
    @Length(3, 35)
    @IsOptional()
    firstName?: string;

    @IsNotEmpty()
    @IsString()
    @Length(3, 35)
    @IsOptional()
    lastName?: string;
    
    @IsEmail()
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @MaxLength(60)
    email?: string;

}