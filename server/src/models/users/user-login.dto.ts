import { IsString, IsNotEmpty, Length } from 'class-validator';

export class UserLoginDTO {
  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  @Length(6, 32)
  password: string;
}
