import { IsNotEmpty, IsString } from 'class-validator';

export class UserBanDTO {
  @IsNotEmpty()
  @IsString()
  banReason: string;

  @IsNotEmpty()
  @IsString()
  expirationDate: string;
}
