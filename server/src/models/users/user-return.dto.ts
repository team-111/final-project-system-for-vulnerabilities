import { Expose } from 'class-transformer';

export class UserReturnDTO {
  @Expose()
  id: string;

  @Expose()
  username: string;

  @Expose()
  firstName: string;

  @Expose()
  lastName: string;

  @Expose()
  email: string;

  @Expose()
  createdOn: Date;

  @Expose()
  userRole: string;

  @Expose()
  avatarUrl: string;

  @Expose()
  banStatus?: any;

  @Expose()
  products?: any;
}
