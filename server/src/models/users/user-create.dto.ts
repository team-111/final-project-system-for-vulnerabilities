import {
  IsNotEmpty,
  IsString,
  Length,
  IsEmail,
} from 'class-validator';

export class CreateUserDTO {
  @IsNotEmpty()
  @IsString()
  @Length(3, 16)
   username: string;

  @IsNotEmpty()
  @IsString()
  @Length(6, 32)
   password: string;

  @IsNotEmpty()
  @IsString()
  @Length(2, 35)
   firstName: string;

  @IsNotEmpty()
  @IsString()
  @Length(2, 35)
   lastName: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @Length(6, 60)
   email: string;

}
