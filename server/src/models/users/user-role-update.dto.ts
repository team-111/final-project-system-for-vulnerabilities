import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { UserRole } from '../enums/user-roles.enum';
export class UserRoleUpdateDTO {
  @IsNotEmpty()
  @IsString()
  @IsEnum(UserRole)
  role: string;
}
