import { Expose } from 'class-transformer';
import { IsString, IsOptional, IsNotEmpty } from 'class-validator';

export class MatchProductDTO {
  @Expose()
  @IsString()
  @IsNotEmpty()
  productName: string;

  @Expose()
  @IsString()
  @IsNotEmpty()
  vendor: string;

  @Expose()
  @IsString()
  @IsNotEmpty()
  version: string;
}
