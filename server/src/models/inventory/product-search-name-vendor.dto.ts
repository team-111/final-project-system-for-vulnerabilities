import { Expose } from 'class-transformer';
import { IsString, IsOptional } from 'class-validator';

export class SearchProductsByNameOrVendorDTO {
  @Expose()
  @IsString()
  @IsOptional()
  productName?: string;

  @Expose()
  @IsString()
  @IsOptional()
  productVendor?: string;
}
