import { Expose } from "class-transformer"

export class InventoryReturnDTO {
    @Expose()
    id: string;

    @Expose()
    productName: string

    @Expose()
    productVendor: string

    @Expose()
    productVersion: string;

    @Expose()
    createdOn: Date;

    @Expose()
    hasCVE: boolean;

    @Expose()
    hasCPE: boolean;

    @Expose()
    cpes?:any;

    @Expose()
    cves?:any;
}