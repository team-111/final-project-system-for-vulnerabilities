import { Expose } from 'class-transformer';
import { IsString, IsNotEmpty } from 'class-validator';

export class AddProductDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  productName: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  productVendor: string;

  @Expose()
  @IsString()
  @IsNotEmpty()
  productVersion: string;
}
