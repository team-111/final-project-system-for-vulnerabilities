import { Expose, Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { SystemVulnerabilitiesError } from 'src/common/exceptions/system-vulnerabilities.error';

export class HasCpeDTO {
  @Expose()
  @IsOptional()
  @Transform(value => {
    if (value === 'false') {
      return false;
    }
    if (value === 'true') {
      return true;
    }
    value === null;
    throw new SystemVulnerabilitiesError(
      'You have to enter true or false',
      404,
    );
  })
  hasCpe: string;
}
