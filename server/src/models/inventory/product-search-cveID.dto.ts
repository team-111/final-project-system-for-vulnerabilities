import { Expose } from "class-transformer";
import { IsString, IsOptional } from "class-validator";

export class SearchCveIdDTO {
    @Expose()
    @IsString()
    @IsOptional()
    cveId?: string;

}