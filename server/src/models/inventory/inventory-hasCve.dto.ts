import { Expose, Transform } from "class-transformer";
import { IsString, IsOptional } from "class-validator";
import { SystemVulnerabilitiesError } from "src/common/exceptions/system-vulnerabilities.error";

export class HasCveDTO {
    @Expose()
    @IsOptional()
    @Transform(value => {
        if (value === 'false') {
          return false;
        }
        if (value === 'true') {
          return true;
        }
        value === null;
        throw new SystemVulnerabilitiesError('You have to enter true or false', 404);
      })
    hasCve: string;
}