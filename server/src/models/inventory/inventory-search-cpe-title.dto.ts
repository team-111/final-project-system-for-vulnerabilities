import { Expose } from "class-transformer";
import { IsString, IsOptional } from "class-validator";

export class SearchCpeByTitleDTO{
    @Expose()
    @IsString()
    @IsOptional()
    title?: string;
}