import { Expose } from "class-transformer";

export class CpeReturnDTO {
    @Expose()
    id: string;
    
    @Expose()
    title: string;

    @Expose()
    cpeUri23: string;

    @Expose()
    version: string;
}