import { DatabaseModule } from './data/database/database.module';
import { CoreModule } from './common/core.module';
import { UsersModule } from './users/users.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CpeModule } from './cpe/cpe.module';
import { CveModule } from './CVE/cve.module';
import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: () => ({
        transport: {
          host: 'smtp.gmail.com',
          port: 587,
          secure: false,
          auth: {
            user: 'no.reply.team11.gp@gmail.com',
            pass: 'Gp123456',
          }
        },
        defaults: {
          from: '"nest-modules" <modules@nestjs.com>',
        },
      })
    }),
    DatabaseModule,
    CoreModule,
    UsersModule,
    ScheduleModule.forRoot(),
    CpeModule,
    CveModule,
  ],

})
export class AppModule { }
