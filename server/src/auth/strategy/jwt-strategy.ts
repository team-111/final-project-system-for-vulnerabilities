import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JWTPayload } from '../payload/jwt-payload';
import { ConfigService } from '@nestjs/config';
import { UsersService } from '../../users/users.service';
import { UserReturnDTO } from '../../models/users/user-return.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersService: UsersService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: JWTPayload): Promise<UserReturnDTO> {
    const foundUser = await this.usersService.findUserByUsername(
      payload.username,
    );

    if (!foundUser) {
      throw new UnauthorizedException();
    }

    return foundUser;
  }
}
