import { SystemVulnerabilitiesError } from '../common/exceptions/system-vulnerabilities.error';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '../data/entities/user.entity';
import { JWTPayload } from './payload/jwt-payload';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { UserLoginDTO } from 'src/models/users/user-login.dto';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(loginUser: UserLoginDTO): Promise<any> {
    const user: User = await this.usersService.findUserByUsername(
      loginUser.username,
    );
    if (!user) {
      throw new UnauthorizedException(
        'User with such username does not exists',
      );
    }

    const validPassword = await this.usersService.validateUserPassword(
      loginUser.password,
      user,
    );

    if (!validPassword) {
      throw new UnauthorizedException('Invalid password!');
    }

    if (user.banStatus.isBanned) {
      throw new SystemVulnerabilitiesError(
        'You can not login - Reason BAN!',
        403,
      );
    }

    const payload: JWTPayload = {
      id: user.id,
      username: user.username,
      avatarUrl: user.avatarUrl,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      createdOn: user.createdOn,
      userRole: user.userRole,
    };

    return {
      token: await this.jwtService.signAsync(payload),
    };
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {
    return this.blacklist.includes(token);
  }
}
