import { AuthGuardWithBlacklisting } from './../common/guards/blacklist.guard';
import {
  Controller,
  Post,
  HttpCode,
  Body,
  HttpStatus,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginDTO } from 'src/models/users/user-login.dto';
import { Token } from '../common/decorators/token.decorator';

@Controller('session')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async login(@Body() loginUser: UserLoginDTO): Promise<{ token: string }> {
    return await this.authService.login(loginUser);
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting)
  async logout(@Token() token: string): Promise<{ message: string }> {
    this.authService.blacklistToken(token);

    return {
      message: 'Successful logout',
    };
  }
}
