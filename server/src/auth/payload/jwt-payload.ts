export class JWTPayload {
  id: string;

  username: string;

  avatarUrl: string;

  firstName: string;

  lastName: string;

  email: string;

  createdOn: Date;

  userRole: string;
}
