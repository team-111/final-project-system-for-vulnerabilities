import { Catch, ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { SystemVulnerabilitiesError } from '../exceptions/system-vulnerabilities.error';

@Catch(SystemVulnerabilitiesError)
export class SystemVulnerabilitiesErrorFilter implements ExceptionFilter {
  catch(exception: SystemVulnerabilitiesError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
