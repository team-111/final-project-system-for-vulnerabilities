export class SystemVulnerabilitiesError extends Error {
    constructor(message?: string, public code?: number) {
        super(message)
    }
}