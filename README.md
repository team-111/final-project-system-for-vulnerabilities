<img src="logo.png" alt="logo" width="400px" style="margin: 20px auto;"/>

#Telerik Final Project Assignment

## Description

Welcome to our **Vulnerability Tracking Software**. Application for finding possible vulnerabilities in software products. The system receives as input a list of software products (the inventory). First to find possible vulnerabilities for the products, the system must employ the CPE dictionary. Second, the system provides a list of CPE candidates that match a product. Once a CPE is assigned to a product, the system searches for CVEs that possibly match the assigned CPE. The system generates alert and allows to send, via SMTP, notifications about the vulnerable software. User could unassign CVEs or delete the Product. Searching different products by different queries or specific CVE is also possible. Every user has his own profile with products history.

## Project Instaling

1. Go to final-project-system-for-vulnerabilities/server folder then in terminal and run:

   - `npm install`

2. Setup MySQL Database

   - Create new Schema.
   - Due to character content of the CPE dictionary it is required Charset/Collation to be utf8mb4 and utf8mb4_unicode_ci respectively.

3. Setup `.env` and `оrmconfig.json` files. They need to be on root level in api folder where is `package.json` and other config files.

   - `.env` file with your settings:

   ```typescript
   PORT = 3000;
   DB_TYPE = mySQL / mariadb;
   DB_HOST = localhost;
   DB_PORT = 3306;
   DB_USERNAME = root;
   DB_PASSWORD = root;
   DB_DATABASE_NAME = system - vulnerabilities - database;
   JWT_SECRET = system - vulnerabilities - database;
   JWT_EXPIRE_TIME = 12 * 60;
   ```

   - `ormconfig.json` file with your settings:

   ```typescript
    {
    "type": "mySQL/mariadb",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "system-vulnerabilities-database",
    "entities": [
        "src/database/entities/**/*.ts"
    ],
    "migrations": [
        "src/database/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/database/entities",
        "migrationsDir": "src/database/migration"
    }
   }
   ```

4. After files are setup go in terminal and run the following commands with the given order:

- `npm run typeorm:run` - this will initialize the migrations
- `npm run seed` - this will initialize an admin user

5. **Go to the following link: https://nvd.nist.gov/products/cpe and download 'Official CPE Dictionary v2.3', zip format and unzip it in the root of the server folder**

6. After files are setup go in terminal and run:

   - `npm run start:dev` or `npm run start`

7. In order to test your api while developing it use the Postman tool. We provided a postman collection with a couple of sample requests that will help you do that.

   - Open the Postman tool
   - Click the Import button and choose the "Vulnerability-Tracking-Software.postman_collection.json" file in the postman folder
   - Use the imported requests to test the API for the different tasks

   <br>

   ![break](./Postman/snapshots/postman-import.png)
   ![break](./Postman/snapshots/postman-collection.png)


    <br/>

8. Go to Postman collection in folder CPEs and run the following request to populate the database with the Dictionary

<br/>

![break](./Postman/snapshots/postman-populate-database.png)

   <br/>

- waiting could take up to 20 min
- in the running terminal you could see the following information about the seed

   <br>

  ![break](server/avatars/seed-start-finish.png)


    <br/>

9. Go to client folder then in terminal and run:

   - `npm install`

10. Now you can start Angular server from terminal with:

- `ng serve --open`

You should see a message on the console **"Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/. Compiled successfully"** and browser should be open.

## Tests

1. You can run tests from client folder with:

   - `npm run test`

**Project have only front-end(Angular) tests on a few components and few services.**
