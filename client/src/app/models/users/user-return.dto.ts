import { InventoryReturnDTO } from '../inventory/product-return.dto';

export class UserReturnDTO {
  id: string;

  username: string;

  firstName: string;

  lastName: string;

  email: string;

  createdOn: Date;

  userRole: string;

  avatarUrl: string;

  banStatus: any;

  products: InventoryReturnDTO[];
}
