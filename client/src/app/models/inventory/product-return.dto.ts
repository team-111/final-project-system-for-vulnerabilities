import { CpeReturnDTO } from '../cpes/cpe-return.dto';
import { CveReturnDTO } from '../cves/cve-return.dto';
import { UserReturnDTO } from '../users/user-return.dto';

export class InventoryReturnDTO {

  id: string;

  productName: string;

  productVendor: string;

  productVersion: string;

  createdOn: Date;

  hasCVE: boolean;

  hasCPE: boolean;

  cves: CveReturnDTO[];

  cpes: CpeReturnDTO;

  user: UserReturnDTO;
}
