export class SearchProductsByNameOrVendorDTO {
  productName?: string;

  productVendor?: string;
}
