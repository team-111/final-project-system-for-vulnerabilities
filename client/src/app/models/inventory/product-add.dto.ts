export class AddProductDTO {
  productName: string;
  productVendor: string;
  productVersion: string;
}
