export class MatchCpeToProductDTO {
  productName: string;
  vendor: string;
  version: string;
}
