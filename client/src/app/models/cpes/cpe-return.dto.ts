export class CpeReturnDTO {
  id: string;
  title: string;
  cpeUri23: string;
  version: string;
}
