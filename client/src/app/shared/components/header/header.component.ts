import { Component, Output, EventEmitter, Input } from '@angular/core';
import { AddProductComponent } from 'src/app/inventory/add-product/add-product.component';
import { MatDialog } from '@angular/material/dialog';
import { SearchSingleCVEComponent } from 'src/app/search-single-cve/search-single-cve.component';
import { AddProductDTO } from 'src/app/models/inventory/product-add.dto';
import { InventoryService } from 'src/app/inventory/services/inventory.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  @Output() logout = new EventEmitter<void>();
  @Input() loggedIn: boolean;
  @Input() loggedUser: UserReturnDTO;

  public userAvatarSrcPrefix = 'http://localhost:3000/avatars/';
  public defaultUserAvatarImagePath = '/assets/logo-avatar.png';

  constructor(
    private readonly inventoryService: InventoryService,
    private readonly notificatorService: NotificatorService,
    private readonly router: Router,
    public dialog: MatDialog
  ) {}

  openDialog() {
    const dialogRef = this.dialog.open(AddProductComponent, {
      width: '60%',
      id: '1',
    });

    const sub = dialogRef.componentInstance.createdProduct.subscribe(
      (product: AddProductDTO) => {
        this.inventoryService.addProduct(product).subscribe({
          next: (data) => {
            this.notificatorService.success(
              'Product successfuly added to inventory'
            );
            this.dialog.closeAll();
            this.router.navigate(['/inventory/product', data.id]);
          },
          error: () =>
            this.notificatorService.error('Error found adding this product'),
        });
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  openDialogCVE() {
    const dialogRef = this.dialog.open(SearchSingleCVEComponent, {
      width: '150%',
    });

    dialogRef.afterClosed().subscribe();
  }

  public triggerLogout() {
    this.logout.emit();
  }
}
