import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { AllUsersResolverService } from './reslovers/all-users.resolver';
import { SingleUserResolverService } from './reslovers/single-user.resolver';
import { SingleUserProductsResolverService } from './reslovers/single-user-products.resolver';
import { AdminGuard } from '../auth/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: AllUsersComponent,
    resolve: { allUsers: AllUsersResolverService },
    canActivate: [AdminGuard]
  },
  {
    path: 'profile/:id',
    component: UserProfileComponent,
    resolve: {
      singleUser: SingleUserResolverService,
      singleUserProducts: SingleUserProductsResolverService,
    },
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRouterModule { }
