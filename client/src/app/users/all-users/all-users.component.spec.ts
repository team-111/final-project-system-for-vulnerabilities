import { AllUsersComponent } from './all-users.component';
import { BehaviorSubject, of } from 'rxjs';
import { ActivatedRoute, Routes } from '@angular/router';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersRouterModule } from '../users-router.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BanUserComponent } from '../ban-user/ban-user.component';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { RouterTestingModule } from '@angular/router/testing';
import { AllUsersResolverService } from '../reslovers/all-users.resolver';
import { AdminGuard } from 'src/app/auth/admin.guard';
import { SingleUserResolverService } from '../reslovers/single-user.resolver';
import { SingleUserProductsResolverService } from '../reslovers/single-user-products.resolver';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
describe('AllUsersComponent', () => {
  let activatedRoute;

  const user = new UserReturnDTO();
  const isLoggedIn$ = new BehaviorSubject(true);
  const loggedUser$ = new BehaviorSubject(user);

  const allNewUsers: UserReturnDTO[] = [
    {
      id: '1',
      username: 'testname',
      firstName: 'test',
      lastName: 'test',
      email: 'test',
      createdOn: new Date('05/05/2020'),
      userRole: 'test',
      avatarUrl: 'test',
      banStatus: 'test',
      products: [new InventoryReturnDTO()]
    },
  ];

  const authService = {
    isLoggedIn$,
    loggedUser$,
  };

  let fixture: ComponentFixture<AllUsersComponent>;
  let component: AllUsersComponent;

  const routes: Routes = [
    {
      path: '',
      component: AllUsersComponent,
      resolve: { allUsers: AllUsersResolverService },
      canActivate: [AdminGuard],
    },
    {
      path: 'profile/:id',
      component: UserProfileComponent,
      resolve: {
        singleUser: SingleUserResolverService,
        singleUserProducts: SingleUserProductsResolverService,
      },
    },
  ];

  beforeEach(async(() => {
    jest.clearAllMocks();

    activatedRoute = {
      data: of(),
    };

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        SharedModule,
        UsersRouterModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [UserProfileComponent, AllUsersComponent, BanUserComponent],
      providers: [
        AuthenticationService,
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
    })
      .overrideProvider(AuthenticationService, { useValue: authService })
      .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
      .compileComponents();

    fixture = TestBed.createComponent(AllUsersComponent);
    component = fixture.componentInstance;
    component.allUsers = allNewUsers;
    fixture.detectChanges();
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  it('should initialize correctly with the data passed from the resolver', () => {
    // Arrange
    activatedRoute.data = of(allNewUsers);

    // Assert & Act
    expect(component.allUsers).toEqual(allNewUsers);
  });

  describe('ngOnInit()', () => {
    it('should update logged on init', (done) => {
      isLoggedIn$.subscribe((state) => {
        expect(component.loggedIn).toBe(true);
        done();
      });
    });

    it('should unsubscribe from isLoggedIn$', (done) => {
      // Arrange & Act
      component.ngOnDestroy();
      isLoggedIn$.next(false);

      // Assert
      isLoggedIn$.subscribe((state) => {
        expect(component.loggedIn).toBe(true);
        done();
      });
    });

    it('should update logged user on init', (done) => {
      loggedUser$.subscribe((state) => {
        expect(component.loggedUser).toBe(user);
        done();
      });
    });

    it('stop receiving data from subscription', () => {
      // Arrange & Act
      component.ngOnInit();
      component.ngOnDestroy();

      // Assert
      expect(component.subscriptionForLoggedUser.closed).toBe(true);
    });
  });
});
