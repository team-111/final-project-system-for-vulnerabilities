import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css'],
})
export class AllUsersComponent implements OnInit, OnDestroy {
  public allUsers: UserReturnDTO[] = [];
  displayedColumns: string[] = [
    'avatarUrl',
    'username',
    'firstName',
    'lastName',
    'email',
    'createdOn',
    'userRole',
    'banStatus',
  ];
  dataSource: MatTableDataSource<UserReturnDTO>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public userAvatarSrcPrefix = 'http://localhost:3000/avatars/';
  public defaultUserAvatarImagePath = '/assets/logo-avatar.png';

  loggedIn: boolean;
  subscriptionForIsUserLogged: Subscription;
  loggedUser: UserReturnDTO;
  subscriptionForLoggedUser: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthenticationService
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.allUsers = data.allUsers;
    });

    this.dataSource = new MatTableDataSource(this.allUsers);
    this.subscriptionForIsUserLogged = this.authService.isLoggedIn$.subscribe(
      (loggedIn) => (this.loggedIn = loggedIn)
    );

    this.subscriptionForLoggedUser = this.authService.loggedUser$.subscribe(
      (loggedUser) => (this.loggedUser = loggedUser)
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subscriptionForIsUserLogged.unsubscribe();
    this.subscriptionForLoggedUser.unsubscribe();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
