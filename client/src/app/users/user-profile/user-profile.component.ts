import { BanUserComponent } from './../ban-user/ban-user.component';
import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { Subscription, Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { UsersService } from '../services/users.service';
import { UpdateUserDTO } from 'src/app/models/users/user-update.dto';
import { StorageService } from 'src/app/core/services/storage.service';
import { ConfirmModalComponent } from 'src/app/shared/components/confirm-modal/confirm-modal.component';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit, OnDestroy {
  public singleUser: UserReturnDTO;
  public singleUserProducts: InventoryReturnDTO[];

  loggedIn: boolean;
  subscriptionForIsUserLogged: Subscription;
  loggedUser: UserReturnDTO;
  subscriptionForLoggedUser: Subscription;
  myProductsPanelState = false;
  inEditMode = false;

  public userAvatarSrcPrefix = 'http://localhost:3000/avatars/';
  public defaultUserAvatarImagePath = '/assets/logo-avatar.png';
  public helper = new JwtHelperService();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  obs: Observable<any>;
  dataSource: MatTableDataSource<InventoryReturnDTO[]>;
  selected = '';
  public products: InventoryReturnDTO[];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthenticationService,
    private changeDetectorRef: ChangeDetectorRef,
    private readonly notificator: NotificatorService,
    private readonly usersService: UsersService,
    private readonly storage: StorageService,
    private readonly route: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: any) => {
      this.singleUser = data.singleUser;
      this.singleUserProducts = data.singleUserProducts;
    });

    this.subscriptionForIsUserLogged = this.authService.isLoggedIn$.subscribe(
      (loggedIn) => (this.loggedIn = loggedIn)
    );

    this.subscriptionForLoggedUser = this.authService.loggedUser$.subscribe(
      (loggedUser) => (this.loggedUser = loggedUser)
    );
    this.dataSource = new MatTableDataSource<any>(this.singleUserProducts);
    this.products = this.singleUserProducts;

    this.changeDetectorRef.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.obs = this.dataSource.connect();
  }

  ngOnDestroy(): void {
    this.subscriptionForIsUserLogged.unsubscribe();
    this.subscriptionForLoggedUser.unsubscribe();
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(BanUserComponent, {
      width: '400px',
    });
    const sub = dialogRef.componentInstance.banStatus.subscribe((data) => {
      this.usersService.banUser(this.singleUser.id, data).subscribe({
        next: () => {
          this.notificator.success('User successfuly banned!');
          this.route.navigate(['/users']);
        },
        error: () =>
          this.notificator.error('Unexpected error banning this user!'),
      });
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  public unbanUser(user: UserReturnDTO) {
    this.usersService.unbanUser(user).subscribe({
      next: () => {
        this.notificator.success('User successfuly unbanned!');
        this.route.navigate(['/users']);
      },
      error: () =>
        this.notificator.error('Unexpected error unbanning this user!'),
    });
  }

  public onFileSelected(file: File): void {
    this.usersService
      .uploadUserAvatar(this.loggedUser.id, file)
      .subscribe({
        next: ({ token }) => {
          this.storage.save('token', token);
          const updatedUser: UserReturnDTO = this.helper.decodeToken(token);
          this.notificator.success('Avatar successfuly updated');

          return this.authService.emitUserData(updatedUser);
        },
        error: () => this.notificator.error('Error found updating user avatar')
      });
  }

  public deleteAvatar(): void {
    this.usersService
      .deleteUserAvatar(this.loggedUser.id)
      .subscribe({
        next: ({ token }) => {
          this.storage.save('token', token);
          const updatedUser: UserReturnDTO = this.helper.decodeToken(token);
          this.notificator.success('Avatar successfuly deleted');

          return this.authService.emitUserData(updatedUser);
        },
        error: () => this.notificator.error('Error found deleting user avatar')
      })
  }

  public toggleMyProductsPanel(): void {
    this.myProductsPanelState = !this.myProductsPanelState;
  }

  public showEdit(): void {
    this.inEditMode = true;
  }

  public cancel(): void {
    this.inEditMode = false;
  }

  public updateUser(): void {
    const userToUpdate: UpdateUserDTO = {
      firstName: this.singleUser.firstName,
      lastName: this.singleUser.lastName,
      email: this.singleUser.email,
    };
    this.usersService.updateUser(userToUpdate).subscribe({
      next: (data) => {
        this.singleUser = { ...this.singleUser, ...data };
        this.notificator.success('User info updated');
        this.inEditMode = false;
      },
      error: () => this.notificator.error('Error found updating user info')
    });
  }

  public deleteUser(userId: string) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '350px',
      data: 'Are you sure you want to delete this user?',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.usersService.deleteUser(userId).subscribe({
          next: () => {
            this.notificator.success('User succesfuly deleted!');
            this.route.navigate(['/users']);
          },
        });
      }
    });
  }

  public promoteToAdmin() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '350px',
      data: 'Are you sure you want to promote this user?',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const userToAdmin = {
          role: 'Admin',
        };
        this.usersService
          .changeUserRole(this.singleUser.id, userToAdmin)
          .subscribe({
            next: (data) => (this.singleUser.userRole = data.userRole),
            error: () =>
              this.notificator.error('Only admins can promote roles!'),
          });
      }
    });
  }

  public demoteToBasic() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '350px',
      data: 'Are you sure you want to demote this user?',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const userToAdmin = {
          role: 'Basic',
        };
        this.usersService
          .changeUserRole(this.singleUser.id, userToAdmin)
          .subscribe((data) => {
            this.singleUser.userRole = data.userRole;
          });
      }
    });
  }
}
