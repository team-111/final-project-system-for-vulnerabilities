import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { UsersService } from '../services/users.service';

@Injectable({ providedIn: 'root' })
export class AllUsersResolverService implements Resolve<UserReturnDTO[]> {
  constructor(private readonly usersService: UsersService) {}

  resolve(): Observable<UserReturnDTO[]> {
    const users = this.usersService.getAllUsers();

    if (!users) {
      throw new Error('Not found users');
    }

    return users;
  }
}
