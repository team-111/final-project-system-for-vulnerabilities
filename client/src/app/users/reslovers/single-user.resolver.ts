import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { UsersService } from '../services/users.service';

@Injectable({ providedIn: 'root' })
export class SingleUserResolverService implements Resolve<UserReturnDTO> {
  constructor(
    private readonly usersService: UsersService,
    private readonly router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<UserReturnDTO> {
    const id = route.params['id'];
    const user = this.usersService.getSingleUser(id);

    return user.pipe(
      // tslint:disable-next-line: no-shadowed-variable
      map((user) => {
        if (user) {
          return user;
        } else {
          this.router.navigate(['/404']);
          return;
        }
      })
    );
  }
}
