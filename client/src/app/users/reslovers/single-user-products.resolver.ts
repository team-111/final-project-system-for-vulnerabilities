import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { InventoryService } from 'src/app/inventory/services/inventory.service';

@Injectable({ providedIn: 'root' })
export class SingleUserProductsResolverService
  implements Resolve<InventoryReturnDTO[]> {
  constructor(private readonly inventoryService: InventoryService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<InventoryReturnDTO[]> {
    const id = route.params['id'];
    const userProducts = this.inventoryService.getProductsForUser(id);

    if (!userProducts) {
      throw new Error('Not found products');
    }
    return userProducts;
  }
}
