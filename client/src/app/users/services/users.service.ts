import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { BanUserDTO } from 'src/app/models/users/user-ban.dto';
import { UpdateUserDTO } from 'src/app/models/users/user-update.dto';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  public baseUrl = `http://localhost:3000/users`;

  constructor(private readonly http: HttpClient) {}

  public getAllUsers(): Observable<UserReturnDTO[]> {
    return this.http.get<UserReturnDTO[]>(`${this.baseUrl}`);
  }

  public getSingleUser(userId: string): Observable<UserReturnDTO> {
    return this.http.get<UserReturnDTO>(`${this.baseUrl}/${userId}`);
  }

  public updateUser(user: UpdateUserDTO): Observable<UserReturnDTO> {
    return this.http.put<UserReturnDTO>(`${this.baseUrl}`, user);
  }

  public deleteUser(userId: string): Observable<UserReturnDTO> {
    return this.http.delete<UserReturnDTO>(`${this.baseUrl}/${userId}`);
  }

  public uploadUserAvatar(userId: any, file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);

    return this.http.post(`${this.baseUrl}/${userId}/avatar`, formData);
  }

  public deleteUserAvatar(userId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${userId}/avatar`);
  }

  public banUser(userId: string, banBody: BanUserDTO): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/${userId}/banstatus`, banBody);
  }

  public unbanUser(user: UserReturnDTO): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${user.id}/banstatus`, user);
  }

  public changeUserRole(
    userId: string,
    role: { role: string }
  ): Observable<UserReturnDTO> {
    return this.http.put<UserReturnDTO>(
      `${this.baseUrl}/${userId}/roles`,
      role
    );
  }
}
