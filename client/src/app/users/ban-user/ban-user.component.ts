import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BanUserDTO } from 'src/app/models/users/user-ban.dto';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AllUsersComponent } from '../all-users/all-users.component';

@Component({
  selector: 'app-ban-user',
  templateUrl: './ban-user.component.html',
  styleUrls: ['./ban-user.component.css'],
})
export class BanUserComponent implements OnInit {
  @Output() banStatus: EventEmitter<BanUserDTO> = new EventEmitter<
    BanUserDTO
  >();
  banReasonField: string;
  banExpirationDate: string;
  banFormGroup = new FormGroup({
    banReasonValidator: new FormControl('', [Validators.required]),
    banExpirationDateValidator: new FormControl('', [Validators.required]),
  });

  constructor(public dialogRef: MatDialogRef<AllUsersComponent>) {}

  ngOnInit(): void {}

  get banReasonValidator() {
    return this.banFormGroup.get('banReasonValidator');
  }

  get banExpirationDateValidator() {
    return this.banFormGroup.get('banExpirationDateValidator');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public toggleBanUser() {
    const banCredentials: BanUserDTO = {
      banReason: this.banReasonField,
      expirationDate: this.banExpirationDate,
    };
    this.banStatus.emit(banCredentials);
  }
}
