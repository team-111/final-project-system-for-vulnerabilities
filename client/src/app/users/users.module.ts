import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SharedModule } from '../shared/shared.module';
import { UsersRouterModule } from './users-router.module';
import { AllUsersComponent } from './all-users/all-users.component';
import { BanUserComponent } from './ban-user/ban-user.component';

@NgModule({
  declarations: [UserProfileComponent, AllUsersComponent, BanUserComponent],
  imports: [CommonModule, SharedModule, UsersRouterModule],
})
export class UsersModule {}
