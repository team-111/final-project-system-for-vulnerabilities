import { Component, OnInit, HostListener } from '@angular/core';
import { CvesService } from '../core/services/cves.service';
import { NotificatorService } from '../core/services/notificator.service';
import { CveReturnDTO } from '../models/cves/cve-return.dto';


@Component({
  selector: 'app-search-single-cve',
  templateUrl: './search-single-cve.component.html',
  styleUrls: ['./search-single-cve.component.css']
})
export class SearchSingleCVEComponent implements OnInit {
  public cveToShow: CveReturnDTO;
  public cveID: string;

  constructor(
    private readonly cvesService: CvesService,
    private readonly notificatorService: NotificatorService
    ) { }
    @HostListener('document:keyup', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
      switch (event.key) {
        case 'Enter':

          this.searchSpecificCveById();
          break;
      }
    }

  ngOnInit(): void {
  }

  public searchSpecificCveById() {
    this.cvesService.getSpecificCveById(this.cveID).subscribe({
      next: (data) => {
        this.cveToShow = data;
      },
      error: () => this.notificatorService.error('No CVE with such ID')
    });
  }

}
