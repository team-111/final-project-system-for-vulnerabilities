import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SearchSingleCVEComponent } from './search-single-cve.component';
import { CvesService } from '../core/services/cves.service';



@NgModule({
  declarations: [SearchSingleCVEComponent],
  imports: [CommonModule, SharedModule],
  providers: [CvesService]
})
export class SearchSingleCveModule { }
