import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificatorService } from './core/services/notificator.service';
import { UserReturnDTO } from './models/users/user-return.dto';
import { AuthenticationService } from './core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  loggedIn: boolean;
  loggedUser: UserReturnDTO;
  subscriptionForIsUserLogged: Subscription;
  subscriptionForLoggedUser: Subscription;

  constructor(
    private readonly authService: AuthenticationService,
    private readonly notificatorService: NotificatorService,
    public routes: Router
  ) {}

  ngOnInit(): void {
    this.subscriptionForIsUserLogged = this.authService.isLoggedIn$.subscribe(
      (loggedIn) => (this.loggedIn = loggedIn)
    );

    this.subscriptionForLoggedUser = this.authService.loggedUser$.subscribe(
      (loggedUser) => (this.loggedUser = loggedUser)
    );
  }

  ngOnDestroy() {
    this.subscriptionForIsUserLogged.unsubscribe();
    this.subscriptionForLoggedUser.unsubscribe();
  }

  public logout() {
    this.authService.logout();
    this.notificatorService.success('Successfuly logged out');
  }
}
