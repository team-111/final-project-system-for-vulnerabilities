import {
  Resolve,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { InventoryService } from '../services/inventory.service';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';

@Injectable({ providedIn: 'root' })
export class AllProductsResolverService implements Resolve<InventoryReturnDTO[]> {
  constructor(private readonly inventoryService: InventoryService) { }

  resolve(acitvatedRoute: ActivatedRouteSnapshot): Observable<any> {
    let products: any;
    if (acitvatedRoute.queryParams.productName) {
      products = this.inventoryService.getProductByNameOrVendor(
        'productName',
        acitvatedRoute.queryParams.productName
      );
    } else if (acitvatedRoute.queryParams.productVendor) {
      products = this.inventoryService.getProductByNameOrVendor(
        'productVendor',
        acitvatedRoute.queryParams.productVendor
      );
    } else if (acitvatedRoute.queryParams.title) {
      products = this.inventoryService.getProductsByCpeTtitle(
        acitvatedRoute.queryParams.title
      );
    } else if (acitvatedRoute.queryParams.cveId) {
      products = this.inventoryService.getProducsByCveId(
        acitvatedRoute.queryParams.cveId
      );
    }
    else {
      products = this.inventoryService.getAllProducts();
    }

    if (!products) {
      throw new Error('Not found products');
    }

    return products;
  }
}
