import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { InventoryService } from '../services/inventory.service';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';

@Injectable({ providedIn: 'root' })
export class SingleProductResolverSerivce implements Resolve<InventoryReturnDTO> {
  constructor(
    private readonly inventoryService: InventoryService,
    private readonly router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<InventoryReturnDTO> {
    // tslint:disable-next-line: no-string-literal
    const id = route.params['id'];
    const product = this.inventoryService.getProductById(id);

    if (!product) {
      throw new Error('Not found post');
    }

    return product.pipe(
      // tslint:disable-next-line: no-shadowed-variable
      map((product) => {
        if (product) {
          return product;
        } else {
          this.router.navigate(['/404']);
          return;
        }
      })
    );
  }
}
