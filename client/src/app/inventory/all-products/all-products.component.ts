import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { InventoryService } from '../services/inventory.service';
import { MatDialog } from '@angular/material/dialog';
import { AddProductDTO } from 'src/app/models/inventory/product-add.dto';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css'],
})
export class AllProductsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public allProducts: InventoryReturnDTO[] = [];
  displayedColumns: string[] = [
    'user',
    'productName',
    'productVendor',
    'productVersion',
    'createdOn',
    'hasCPE',
    'hasCVE',
  ];
  dataSource: MatTableDataSource<InventoryReturnDTO>;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly notificatorService: NotificatorService,
    private readonly inventoryService: InventoryService,
    public dialog: MatDialog,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.allProducts = data.allProducts;
    });
    this.dataSource = new MatTableDataSource(this.allProducts);

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.openDialog();
  }

  openDialog() {
    const dialogRef = this.dialog.getDialogById('1');

    const sub = dialogRef?.componentInstance.createdProduct.subscribe(
      (product: AddProductDTO) => {
        this.inventoryService
          .addProduct(product)
          .subscribe((addedProduct: InventoryReturnDTO) => {
            this.allProducts.push(addedProduct);
            this.dataSource.connect().next(this.allProducts);
          });
      }
    );

    dialogRef?.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteProduct(product: InventoryReturnDTO): void {
    this.inventoryService.deleteProduct(product.id).subscribe({
      next: (deletedProduct: InventoryReturnDTO) => {
        this.allProducts = this.allProducts.filter(
          (productToView: InventoryReturnDTO) =>
            productToView.id !== deletedProduct.id
        );
        this.notificatorService.success('Product successfully deleted');
      },
      error: () =>
        this.notificatorService.error('Error found deleting this product'),
    });
  }

  openSingleProduct(product: InventoryReturnDTO) {
    this.router.navigate(['/inventory/product', product.id]);
  }
}
