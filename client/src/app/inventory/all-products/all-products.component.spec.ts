import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { Routes, ActivatedRoute, Router } from '@angular/router';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { MatDialog } from '@angular/material/dialog';
import { InventoryComponent } from '../inventory/inventory.component';
import { AllProductsResolverService } from '../resolvers/all-products.resolver';
import { ProductDetailsComponent } from '../product-details/product-details.component';
import { SingleProductResolverSerivce } from '../resolvers/single-product.resolver';
import { InventoryRouterModule } from '../inventory-router.module';
import { RouterTestingModule } from '@angular/router/testing';
import { InventoryService } from '../services/inventory.service';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { CpeReturnDTO } from 'src/app/models/cpes/cpe-return.dto';
import { CveReturnDTO } from 'src/app/models/cves/cve-return.dto';
import { AllProductsComponent } from './all-products.component';
import { InventoryModule } from '../inventory.module';

describe('AllProductsComponent', () => {
  let inventoryService;
  let notificationService;
  let dialog;
  let activatedRoute;
  let router;

  const allNewProducts: InventoryReturnDTO[] = [{
    id: '1',
    productName: 'test',
    productVendor: 'test',
    productVersion: 'test',
    createdOn: new Date(),
    hasCVE: true,
    hasCPE: false,
    cves: [new CveReturnDTO()],
    cpes: new CpeReturnDTO(),
    user: new UserReturnDTO(),
  }];

  const inventoryRoutes: Routes = [
    {
      path: '',
      component: InventoryComponent,
      pathMatch: 'full',
      resolve: { allPosts: AllProductsResolverService },
    },

    {
      path: 'product/:id',
      component: ProductDetailsComponent,
      resolve: { post: SingleProductResolverSerivce },
    },
  ];

  let fixture: ComponentFixture<AllProductsComponent>;
  let component: AllProductsComponent;

  beforeEach(async(() => {
    jest.clearAllMocks();


    activatedRoute = {
      data: of(),
    };

    inventoryService = {
      addProduct() { },
      deleteProduct() { },
    };

    notificationService = {
      error() { },
      success() { }
    };

    router = {
      navigate() { }
    };


    dialog = {
      open() { },
      getDialogById() { }
    };

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        InventoryRouterModule,
        BrowserAnimationsModule,
        InventoryModule,
        RouterTestingModule.withRoutes(inventoryRoutes)
      ],
      declarations: [

      ],
      providers: [
        AuthenticationService, InventoryService, NotificatorService,
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: Router, useValue: router }
      ]
    })
      .overrideProvider(InventoryService, { useValue: inventoryService })
      .overrideProvider(NotificatorService, { useValue: notificationService })
      .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
      .overrideProvider(MatDialog, { useValue: dialog })
      .compileComponents();

    fixture = TestBed.createComponent(AllProductsComponent);
    component = fixture.componentInstance;
    component.allProducts = allNewProducts;
    fixture.detectChanges();

    router = TestBed.inject(Router);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  it('should initialize correctly with the data passed from the resolver', () => {
    // Arrange
    activatedRoute.data = of(allNewProducts);

    // Assert & Act
    expect(component.allProducts).toEqual(allNewProducts);

  });

  describe('deleteProduct()', () => {
    it('should call the inventoryService.deleteProduct() once with correct parameters', () => {
      // Arrange
      const product: InventoryReturnDTO = {
        id: '1',
        productName: 'test',
        productVendor: 'test',
        productVersion: 'test',
        createdOn: new Date(),
        hasCVE: true,
        hasCPE: false,
        cves: [new CveReturnDTO()],
        cpes: new CpeReturnDTO(),
        user: new UserReturnDTO(),
      };

      const spy = jest
        .spyOn(inventoryService, 'deleteProduct')
        .mockReturnValue(of(product));

      const mockedProductToDelete = { id: 1 };
      component.allProducts = [mockedProductToDelete as any];

      const mockedDeleteProductData = { id: 'fake id' };

      // Act
      component.deleteProduct(mockedDeleteProductData as any);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockedDeleteProductData.id);
    });

    it('should call inventoryService.success() once with correct message when the product is deleted successfuly', () => {
      // Arrange
      const product: InventoryReturnDTO = {
        id: '1',
        productName: 'test',
        productVendor: 'test',
        productVersion: 'test',
        createdOn: new Date(),
        hasCVE: true,
        hasCPE: false,
        cves: [new CveReturnDTO()],
        cpes: new CpeReturnDTO(),
        user: new UserReturnDTO(),
      };
      const spy = jest
        .spyOn(inventoryService, 'deleteProduct')
        .mockReturnValue(of(product));

      const successSpy = jest.spyOn(notificationService, 'success');

      // Act
      component.deleteProduct(product);

      // Assert
      expect(successSpy).toHaveBeenCalledTimes(1);
      expect(successSpy).toHaveBeenCalledWith('Product successfully deleted');
    });

    it('should call inventoryService.error() once with correct message when user is not able to delete products', () => {
      // Arrange
      const product: InventoryReturnDTO = {
        id: '1',
        productName: 'test',
        productVendor: 'test',
        productVersion: 'test',
        createdOn: new Date(),
        hasCVE: true,
        hasCPE: false,
        cves: [new CveReturnDTO()],
        cpes: new CpeReturnDTO(),
        user: new UserReturnDTO(),
      };
      const spy = jest
        .spyOn(inventoryService, 'deleteProduct')
        .mockReturnValue(throwError('mocked delete fail error'));

      const successSpy = jest.spyOn(notificationService, 'error');


      // Act
      component.deleteProduct(product);

      // Assert
      expect(successSpy).toHaveBeenCalledTimes(1);
      expect(successSpy).toHaveBeenCalledWith('You need to login to delete products');
    });
  });

  describe('openSingleProduct()', () => {
    it('should navigate to single product', () => {
      // Arrange
      const product: InventoryReturnDTO = {
        id: '1',
        productName: 'test',
        productVendor: 'test',
        productVersion: 'test',
        createdOn: new Date(),
        hasCVE: true,
        hasCPE: false,
        cves: [new CveReturnDTO()],
        cpes: new CpeReturnDTO(),
        user: new UserReturnDTO(),
      };
      const posts = [product];
      activatedRoute.data = of({ posts });

      // Act
      const spy = jest.spyOn(router, 'navigate');

      component.openSingleProduct(product);
      // Assert
      expect(router.navigate).toHaveBeenCalledWith(['product', product.id]);

    });
  });
});
