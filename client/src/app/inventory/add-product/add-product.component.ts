import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { HeaderComponent } from '../../shared/components/header/header.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddProductDTO } from 'src/app/models/inventory/product-add.dto';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  @Output() createdProduct: EventEmitter<AddProductDTO> = new EventEmitter<
  AddProductDTO
>();
  public productName: string;
  public productVendor = '';
  public productVersion: string;

  public inventoryProduct: InventoryReturnDTO;


  constructor(
    public dialogRef: MatDialogRef<HeaderComponent>,
    // tslint:disable-next-line: variable-name
    private _formBuilder: FormBuilder,
    public readonly router: Router,
  ) {}

  productNameFormGroup: FormGroup;
  productVendorFormGroup: FormGroup;
  productVersionFormGroup: FormGroup;

  ngOnInit() {
    this.productNameFormGroup = this._formBuilder.group({
      productNameCtrl: ['', Validators.required],
    });
    this.productVendorFormGroup = this._formBuilder.group({
      productVendorCtrl: ['', Validators.required],
    });
    this.productVersionFormGroup = this._formBuilder.group({
      productVersionCtrl: ['', Validators.required],
    });
  }

  public addProduct(): any {
    const product: AddProductDTO = {
      productName: this.productName,
      productVendor: this.productVendor,
      productVersion: this.productVersion,
    };
    this.createdProduct.emit(product);
  }
}
