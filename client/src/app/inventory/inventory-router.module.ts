import { InventoryComponent } from './inventory/inventory.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { AllProductsResolverService } from './resolvers/all-products.resolver';
import { SingleProductResolverSerivce } from './resolvers/single-product.resolver';

const inventoryRoutes: Routes = [
  {
    path: '',
    component: InventoryComponent,
    pathMatch: 'full',
    resolve: { allProducts: AllProductsResolverService },
  },
  {
    path: 'product/:id',
    component: ProductDetailsComponent,
    resolve: {
      singleProduct: SingleProductResolverSerivce
    }
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(inventoryRoutes)],
  exports: [RouterModule],
})
export class InventoryRouterModule { }
