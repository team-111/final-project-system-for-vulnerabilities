import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { InventoryService } from '../services/inventory.service';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { Router } from '@angular/router';



@Component({
  selector: 'app-products-without-cves',
  templateUrl: './products-without-cves.component.html',
  styleUrls: ['./products-without-cves.component.css'],
})
export class ProductsWithoutCVEsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public allProductsWithoutCVEs: InventoryReturnDTO[] = [];

  displayedColumns: string[] = [
    'user',
    'productName',
    'productVendor',
    'productVersion',
    'createdOn',
    'hasCPE',
    'hasCVE',
  ];
  dataSource: MatTableDataSource<InventoryReturnDTO>;


  constructor(
    private readonly inventoryService: InventoryService,
    private readonly router: Router
  ) {

  }

  ngOnInit() {
    this.inventoryService.getAllProductsWithoutCVEs().subscribe({
      next: (data) => {
        this.allProductsWithoutCVEs = data;
      }
    });

    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.allProductsWithoutCVEs);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, 2000);
  }

  openSingleProduct(product: InventoryReturnDTO) {
    this.router.navigate(['/inventory/product', product.id]);
  }
}
