import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { AddProductDTO } from 'src/app/models/inventory/product-add.dto';

@Injectable({
  providedIn: 'root',
})
export class InventoryService {
  public baseUrl = `http://localhost:3000/products`;

  public constructor(private readonly http: HttpClient) {}

  public getAllProducts(): Observable<InventoryReturnDTO[]> {
    return this.http.get<InventoryReturnDTO[]>(this.baseUrl);
  }

  public getAllProductsWithCVEs(): Observable<InventoryReturnDTO[]> {
    const url = `${this.baseUrl}/cves?hasCve=true`;
    return this.http.get<InventoryReturnDTO[]>(url);
  }

  public getAllProductsWithoutCVEs(): Observable<InventoryReturnDTO[]> {
    const url = `${this.baseUrl}/cves?hasCve=false`;
    return this.http.get<InventoryReturnDTO[]>(url);
  }

  public getProductById(productId: string): Observable<InventoryReturnDTO> {
    const url = `${this.baseUrl}/single/${productId}`;
    return this.http.get<InventoryReturnDTO>(url);
  }

  public addProduct(product: AddProductDTO): Observable<InventoryReturnDTO> {
    return this.http.post<InventoryReturnDTO>(this.baseUrl, product);
  }

  public deleteProduct(productId: string): Observable<InventoryReturnDTO> {
    const url = `${this.baseUrl}/${productId}`;
    return this.http.delete<InventoryReturnDTO>(url);
  }

  public getProductByNameOrVendor(
    options: any,
    value: string
  ): Observable<InventoryReturnDTO[]> {
    const url = `${this.baseUrl}?${options}=${value}`;
    return this.http.get<InventoryReturnDTO[]>(url);
  }

  public getProductsByCpeTtitle(
    value: string
  ): Observable<InventoryReturnDTO[]> {
    const url = `${this.baseUrl}/cpes?title=${value}`;
    return this.http.get<InventoryReturnDTO[]>(url);
  }

  public getProducsByCveId(value: string): Observable<InventoryReturnDTO[]> {
    const url = `${this.baseUrl}/cve?cveId=${value}`;
    return this.http.get<InventoryReturnDTO[]>(url);
  }

  public getAllProductsCount(): Observable<number> {
    const url = `${this.baseUrl}/count`;

    return this.http.get<number>(url);
  }

  public getAllProductsWithOrWithoutCVEsCount(
    hasCve: boolean
  ): Observable<number> {
    const url = `${this.baseUrl}/cves/count?hasCve=${hasCve}`;

    return this.http.get<number>(url);
  }

  public getAllProductsWithOrWithoutCPEsCount(
    hasCpe: boolean
  ): Observable<number> {
    const url = `${this.baseUrl}/cpe/count?hasCpe=${hasCpe}`;

    return this.http.get<number>(url);
  }

  public getProductsForUser(userId: string): Observable<InventoryReturnDTO[]> {
    const url = `${this.baseUrl}/user/${userId}`;

    return this.http.get<InventoryReturnDTO[]>(url);
  }
}
