import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryComponent } from './inventory/inventory.component';
import { SharedModule } from '../shared/shared.module';
import { InventoryRouterModule } from './inventory-router.module';
import { AllProductsComponent } from './all-products/all-products.component';
import { ProductsWithCVEsComponent } from './products-with-cves/products-with-cves.component';
import { ProductsWithoutCVEsComponent } from './products-without-cves/products-without-cves.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { AllProductsResolverService } from './resolvers/all-products.resolver';
import { SingleProductResolverSerivce } from './resolvers/single-product.resolver';
import { AddProductComponent } from './add-product/add-product.component';

@NgModule({
  declarations: [
    InventoryComponent,
    AllProductsComponent,
    ProductsWithCVEsComponent,
    ProductsWithoutCVEsComponent,
    ProductDetailsComponent,
    AddProductComponent,
  ],
  providers: [AllProductsResolverService, SingleProductResolverSerivce],
  imports: [CommonModule, SharedModule, InventoryRouterModule],
})
export class InventoryModule {}
