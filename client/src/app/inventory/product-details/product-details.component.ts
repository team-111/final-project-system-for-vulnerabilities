import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  OnDestroy,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmModalComponent } from 'src/app/shared/components/confirm-modal/confirm-modal.component';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { CpesService } from 'src/app/core/services/cpes.service';
import { CvesService } from 'src/app/core/services/cves.service';
import { CpeReturnDTO } from 'src/app/models/cpes/cpe-return.dto';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { CveReturnDTO } from 'src/app/models/cves/cve-return.dto';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { InventoryService } from '../services/inventory.service';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  obs: Observable<any>;
  dataSource: MatTableDataSource<CpeReturnDTO>;
  public singleProduct: InventoryReturnDTO;
  public matchedCpes: CpeReturnDTO[];
  public matchedCves: CveReturnDTO[];
  public matchedCvesIds: string;

  loggedUser: UserReturnDTO;
  subscriptionForLoggedUser: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private readonly cpesService: CpesService,
    private readonly cvesSserivce: CvesService,
    private readonly notificatorService: NotificatorService,
    private changeDetectorRef: ChangeDetectorRef,
    private readonly inventoryService: InventoryService,
    private router: Router,
    private readonly authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: any) => {
      this.singleProduct = data.singleProduct;
    });

    this.subscriptionForLoggedUser = this.authService.loggedUser$.subscribe(
      (loggedUser) => (this.loggedUser = loggedUser)
    );

    if (!this.singleProduct.hasCPE) {
      this.cpesService
        .matchCpeWithProduct(this.singleProduct.id)
        .subscribe((data) => {
          this.matchedCpes = data;
          this.dataSource = new MatTableDataSource<CpeReturnDTO>(
            this.matchedCpes
          );
          this.changeDetectorRef.detectChanges();
          this.dataSource.paginator = this.paginator;
          this.obs = this.dataSource.connect();
        });
    }

    if (this.singleProduct?.cpes?.id) {
      this.getCveForCpe();
    }
  }
  ngOnDestroy(): void {
    this.subscriptionForLoggedUser.unsubscribe();

    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  public getCveForCpe(): void {
    this.cvesSserivce
      .getCvesForCpe(this.singleProduct?.cpes?.id)
      .subscribe((data) => {
        this.matchedCves = data;
        this.matchedCvesIds = this.singleProduct.cves
          ?.map((el) => el.cveId)
          .join(', ');
      });
  }

  public assignCpe(cpeId: string): void {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '25%',
      data: `This step is irreversible! Are you sure you want to assign this CPE to this product?
       An email will be send to notify the system administrators!`,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.cpesService
          .assignCpeToProduct(cpeId, this.singleProduct.id)
          .subscribe({
            next: (data) => {
              this.singleProduct.cpes = data.cpes;
              this.singleProduct.hasCPE = true;
              this.populateCvesForCpe();
              this.notificatorService.success(
                'CPE successfuly assigned to this product'
              );
            },
            error: () =>
              this.notificatorService.error(
                'This CPE is already assigned to another product'
              ),
          });
      }
    });
  }

  public populateCvesForCpe(): void {
    const matchedCpeUri = {
      matchedUri: this.singleProduct.cpes.cpeUri23,
    };
    this.cvesSserivce
      .populateCVEsForCPE(matchedCpeUri)
      .subscribe(() => this.getCveForCpe());
  }

  public deleteProduct() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '20%',
      data: 'Are you sure you want to delete this product?',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.inventoryService.deleteProduct(this.singleProduct.id).subscribe({
          next: () => {
            this.notificatorService.success('Product successfuly deleted');
            this.router.navigate(['/inventory']);
          },
        });
      }
    });
  }

  public assignCve(cveId: string): void {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '25%',
      data: `Are you sure you want to assign this CVE to this product? An email will be send to notify the system administrators!`,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.cvesSserivce
          .assignCveToProduct(cveId, this.singleProduct.id)
          .subscribe({
            next: (data) => {
              this.matchedCvesIds = data.cves?.map((el) => el.cveId).join(', ');
              this.notificatorService.success(
                'This CVE successfuly assigned to this product'
              );
            },
            error: () =>
              this.notificatorService.error(
                'This CVE is already assigned to this product'
              ),
          });
      }
    });
  }

  public unassignCve(cveId: string): void {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '25%',
      data: `Are you sure you want to unassign this CVE?`,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.cvesSserivce
          .unassignCveFromProduct(cveId, this.singleProduct.id)
          .subscribe({
            next: (data) => {
              this.matchedCvesIds = this.matchedCvesIds
                .split(', ')
                .filter((el) => el !== data.cveId)
                .join(', ');
              this.notificatorService.success(
                'This CVE was successfuly unassigned from this product'
              );
            },
            error: () => this.notificatorService.error('This CVE is not found'),
          });
      }
    });
  }
}
