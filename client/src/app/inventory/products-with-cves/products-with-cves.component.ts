import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { InventoryService } from '../services/inventory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products-with-cves',
  templateUrl: './products-with-cves.component.html',
  styleUrls: ['./products-with-cves.component.css'],
})
export class ProductsWithCVEsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public allProductsWithCVEs: InventoryReturnDTO[] = [];
  displayedColumns: string[] = [
    'user',
    'productName',
    'productVendor',
    'productVersion',
    'createdOn',
    'hasCPE',
    'hasCVE',
  ];

  dataSource: MatTableDataSource<InventoryReturnDTO>;

  constructor(
    private readonly inventoryService: InventoryService,
    private readonly router: Router
  ) {

  }

  ngOnInit() {
    this.inventoryService.getAllProductsWithCVEs().subscribe({
      next: (data) => {
        this.allProductsWithCVEs = data;
      }
    });

    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.allProductsWithCVEs);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, 2000);
  }
  openSingleProduct(product: InventoryReturnDTO) {
    this.router.navigate(['/inventory/product', product.id]);
  }
}
