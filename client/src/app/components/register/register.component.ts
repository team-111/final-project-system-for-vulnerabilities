import { Component, HostListener } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';
import { UserRegisterDTO } from 'src/app/models/users/user-register.dto';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  hide = true;

  constructor(
    private readonly authService: AuthenticationService,
    private readonly notificatorService: NotificatorService,
    private readonly router: Router,
  ) { }

  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);
  firstName = new FormControl('', [Validators.required]);
  lastName = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);

  registerUsername: string;
  registerPassword: string;
  registerFirstName: string;
  registerLastName: string;
  registerEmail: string;

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case 'Enter':

        this.registerUser();
        break;
    }
  }

  getErrorMessageUsername() {
    if (this.username.hasError('required')) {
      return 'You must enter an username';
    }
  }
  getErrorMessagePassword() {
    if (this.password.hasError('required')) {
      return 'You must enter a password';
    }
  }
  getErrorMessageFirstName() {
    if (this.firstName.hasError('required')) {
      return 'You must enter first name';
    }
  }
  getErrorMessageLastName() {
    if (this.lastName.hasError('required')) {
      return 'You must enter last name';
    }
  }

  getErrorMessageEmail() {
    if (this.email.hasError('required')) {
      return 'You must enter an email';
    }
  }

  public registerUser() {
    const user: UserRegisterDTO = {
      username: this.registerUsername,
      password: this.registerPassword,
      firstName: this.registerFirstName,
      lastName: this.registerLastName,
      email: this.registerEmail,
    };
    this.authService.register(user).subscribe(
      () => {
        this.notificatorService.success('User successfuly registered!');
        this.router.navigate(['/dashboard']);
      },
      () => {
        this.notificatorService.error('Registration failed!');
      }
    );
  }
}
