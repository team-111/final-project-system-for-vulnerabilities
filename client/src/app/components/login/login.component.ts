import { Component, HostListener } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  hide = true;
  password = new FormControl('', [Validators.required]);
  username = new FormControl('', [Validators.required]);
  loginUsername: string;
  loginPassword: string;

  constructor(
    private readonly authService: AuthenticationService,
    private readonly router: Router,
    private readonly notificatorService: NotificatorService
  ) {}
  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case 'Enter':
        const userToLogin = {
          username: this.loginUsername,
          password: this.loginPassword,
        };

        this.authService.login(userToLogin).subscribe(
          () => {
            this.notificatorService.success('Successfuly logged in');
            this.router.navigate(['/dashboard']);
          },
          () => this.notificatorService.error('Wrong username or password')
        );
        break;
    }
  }

  getErrorMessage() {
    if (this.username.hasError('required')) {
      return 'You must enter a value';
    }
    return this.username.hasError('username') ? 'Not a valid username' : '';
  }
  getErrorMessagePassword() {
    if (this.password.hasError('required')) {
      return 'You must enter a value';
    }
    return this.password.hasError('password') ? 'Not a valid password' : '';
  }

  public login() {
    const userToLogin = {
      username: this.loginUsername,
      password: this.loginPassword,
    };

    this.authService.login(userToLogin).subscribe(
      () => {
        this.notificatorService.success('Successfuly logged in');
        this.router.navigate(['/dashboard']);
      },
      () => this.notificatorService.error('Wrong username or password')
    );
  }
}
