import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserLoginDTO } from 'src/app/models/users/user-login.dto';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from './storage.service';
import { UserReturnDTO } from 'src/app/models/users/user-return.dto';
import { UserRegisterDTO } from 'src/app/models/users/user-register.dto';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  private readonly helper = new JwtHelperService();

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserLoggedIn()
  );
  private readonly loggedUserSubject$ = new BehaviorSubject<UserReturnDTO>(
    this.loggedUser()
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router,
  ) {
  }

  public emitUserData(user: UserReturnDTO): void {
    this.loggedUserSubject$.next(user);
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<UserReturnDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  public register(user: UserRegisterDTO): Observable<UserReturnDTO> {
    return this.http.post<UserReturnDTO>('http://localhost:3000/users', user);
  }

  public login(user: UserLoginDTO): Observable<{ token: string }> {
    return this.http
      .post<{ token: string }>('http://localhost:3000/session', user)
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser = this.helper.decodeToken(token);
            this.storage.save('token', token);

            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);
          } catch (error) { }
        })
      );
  }

  public logout() {
    this.storage.save('token', '');
    this.isLoggedInSubject$.next(false);
    this.loggedUserSubject$.next(null);

    this.router.navigate(['login']);
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  private loggedUser(): UserReturnDTO {
    try {
      return this.helper.decodeToken(this.storage.read('token'));
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
