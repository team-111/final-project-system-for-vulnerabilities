import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { CpesService } from './cpes.service';
import { of } from 'rxjs';

const baseUrl = `http://localhost:3000/cpes`;
let http;
let service: CpesService;

describe('CpesService', () => {
  http = {
    get() {},
    post() {},
  };

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [CpesService, HttpClient],
    }).overrideProvider(HttpClient, { useValue: http });

    service = TestBed.inject(CpesService);
  });

  it('should be initialized', () => {
    expect(service).toBeDefined();
  });

  describe('matchCpeWithProduct()', () => {
    it('should call the httpclient.get() method once with correct parameters', (done) => {
      // Arrange
      const mockProductId = '1';
      const url = `${baseUrl}/products/${mockProductId}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.matchCpeWithProduct(mockProductId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockProductId = '1';
      const url = `${baseUrl}/products/${mockProductId}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act
      const result = service.matchCpeWithProduct(mockProductId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('assignCpeToProduct()', () => {
    it('should call the httpClient.post() method once with correct parameters', (done) => {
      // Arrange
      const mockCpeId = '1';
      const mockProductId = '2';
      const url = `${baseUrl}/${mockCpeId}/products/${mockProductId}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(http, 'post').mockReturnValue(returnValue);

      // Act && Assert
      service.assignCpeToProduct(mockCpeId, mockProductId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, {});

        done();
      });
    });
    it('should return the result from the httpClient.post() method', () => {
      // Arrange
      const mockCpeId = '1';
      const mockProductId = '2';
      const url = `${baseUrl}/${mockCpeId}/products/${mockProductId}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(http, 'post').mockReturnValue(returnValue);

      // Act
      const result = service.assignCpeToProduct(mockCpeId, mockProductId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('populateDictionary()', () => {
    it('should call the httpClient.post() method once with correct parameters', (done) => {
      // Arrange
      const url = `${baseUrl}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(http, 'post').mockReturnValue(returnValue);

      // Act && Assert
      service.populateDictionary().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, {});

        done();
      });
    });
    it('should return the result from the httpClient.post() method', () => {
      // Arrange
      const url = `${baseUrl}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(http, 'post').mockReturnValue(returnValue);

      // Act
      const result = service.populateDictionary();

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('getAllCPEsCount()', () => {
    it('should call the httpClient.get() method once with correct parameters', (done) => {
      // Arrange
      const url = `${baseUrl}/count`;
      const returnValue = of(5);

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getAllCPEsCount().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const url = `${baseUrl}/count`;
      const returnValue = of(5);

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act
      const result = service.getAllCPEsCount();
      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('getAllCpeNamesCount()', () => {
    it('should call the httpClient.get() method once with correct parameters', (done) => {
      // Arrange
      const url = `${baseUrl}/names/count`;
      const returnValue = of(5);

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getAllCpeNamesCount().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const url = `${baseUrl}/names/count`;
      const returnValue = of(5);

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act
      const result = service.getAllCpeNamesCount();
      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('getAllCpeVendorsCount()', () => {
    it('should call the httpClient.get() method once with correct parameters', (done) => {
      // Arrange
      const url = `${baseUrl}/vendors/count`;
      const returnValue = of(5);

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getAllCpeVendorsCount().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const url = `${baseUrl}/vendors/count`;
      const returnValue = of(5);

      const spy = jest.spyOn(http, 'get').mockReturnValue(returnValue);

      // Act
      const result = service.getAllCpeVendorsCount();
      // Assert
      expect(result).toEqual(returnValue);
    });
  });
});
