import { InventoryReturnDTO } from './../../models/inventory/product-return.dto';
import { MatchedUriDTO } from './../../models/cves/cve-matchedUri.dto';
import { CveReturnDTO } from './../../models/cves/cve-return.dto';
import { CvesService } from './cves.service';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
const baseUrl = `http://localhost:3000/cves`;
let http;
let service: CvesService;

describe('CvesService', () => {
  http = {
    get() {},
    post() {},
  };

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [CvesService, HttpClient],
    }).overrideProvider(HttpClient, { useValue: http });

    service = TestBed.inject(CvesService);
  });

  it('should be initialized', () => {
    expect(service).toBeDefined();
  });

  describe('getAllCves()', () => {
    it('should call the httpClient.get() method once with correct parameters', () => {
      //Arrange
      const spy = jest
        .spyOn(http, 'get')
        .mockImplementation(() => of([new CveReturnDTO()]));

      //Act
      service.getAllCves();

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(baseUrl);
    });

    it('should return the result from the httpClient.get() method', (done) => {
      // Arrange
      const spy = jest
        .spyOn(http, 'get')
        .mockImplementation(() => of([new CveReturnDTO()]));

      // Act
      service.getAllCves().subscribe((data) => {
        // Assert
        expect(data).toEqual([new CveReturnDTO()]);
        done();
      });
    });
  });

  describe('getSpecificCveById()', () => {
    it('should call the httpClient.get() method once with correct parameters', () => {
      //Arrange
      const mockedId = '5';
      const url = `${baseUrl}/${mockedId}`;
      const spy = jest
        .spyOn(http, 'get')
        .mockImplementation(() => of(new CveReturnDTO()));

      //Act
      service.getSpecificCveById(mockedId);

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('should return the result from the httpClient.get() method', (done) => {
      //Arrange
      const mockedId = '5';
      const url = `${this.baseUrl}/${mockedId}`;
      const spy = jest
        .spyOn(http, 'get')
        .mockImplementation(() => of(new CveReturnDTO()));

      // Act
      service.getSpecificCveById(mockedId).subscribe((data) => {
        // Assert
        expect(data).toEqual(new CveReturnDTO());
        done();
      });
    });
  });

  describe('getCvesForCpe()', () => {
    it('should call the httpClient.get() method once with correct parameters', () => {
      //Arrange
      const mockedId = '5';
      const url = `${baseUrl}/cpes/${mockedId}`;
      const spy = jest
        .spyOn(http, 'get')
        .mockImplementation(() => of([new CveReturnDTO()]));

      //Act
      service.getCvesForCpe(mockedId);

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('should return the result from the httpClient.get() method', (done) => {
      //Arrange
      const mockedId = '5';
      const url = `${baseUrl}/cpes/${mockedId}`;
      const spy = jest
        .spyOn(http, 'get')
        .mockImplementation(() => of([new CveReturnDTO()]));

      // Act
      service.getCvesForCpe(mockedId).subscribe((data) => {
        // Assert
        expect(data).toEqual([new CveReturnDTO()]);
        done();
      });
    });
  });

  describe('populateCVEsForCPE()', () => {
    it('should call the httpClient.post() method once with correct parameters', () => {
      //Arrange
      const mockedUri = new MatchedUriDTO();
      const spy = jest
        .spyOn(http, 'post')
        .mockImplementation(() => of([new CveReturnDTO()]));

      //Act
      service.populateCVEsForCPE(mockedUri);

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(baseUrl, mockedUri);
    });

    it('should return the result from the httpClient.post() method', (done) => {
      //Arrange
      const mockedUri = new MatchedUriDTO();
      const spy = jest
        .spyOn(http, 'post')
        .mockImplementation(() => of([new CveReturnDTO()]));

      // Act
      service.populateCVEsForCPE(mockedUri).subscribe((data) => {
        // Assert
        expect(data).toEqual([new CveReturnDTO()]);
        done();
      });
    });
  });

  describe('assignCveToProduct()', () => {
    it('should call the httpClient.post() method once with correct parameters', () => {
      //Arrange
      const mockedCveId = '5';
      const mockedProductId = '3';
      const url = `${baseUrl}/${mockedCveId}/products/${mockedProductId}`;
      const spy = jest
        .spyOn(http, 'post')
        .mockImplementation(() => of(new InventoryReturnDTO()));

      //Act
      service.assignCveToProduct(mockedCveId, mockedProductId);

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, {});
    });

    it('should return the result from the httpClient.post() method', (done) => {
      //Arrange
      const mockedCveId = '5';
      const mockedProductId = '3';
      const url = `${baseUrl}/${mockedCveId}/products/${mockedProductId}`;
      const spy = jest
        .spyOn(http, 'post')
        .mockImplementation(() => of(new InventoryReturnDTO()));

      // Act
      service
        .assignCveToProduct(mockedCveId, mockedProductId)
        .subscribe((data) => {
          // Assert
          expect(data).toEqual(new InventoryReturnDTO());
          done();
        });
    });
  });
});
