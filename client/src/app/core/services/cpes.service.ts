import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CpeReturnDTO } from 'src/app/models/cpes/cpe-return.dto';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';

@Injectable({
  providedIn: 'root'
})
export class CpesService {
  public baseUrl = `http://localhost:3000/cpes`;
  constructor(private readonly http: HttpClient) { }

  public matchCpeWithProduct(productId: string): Observable<CpeReturnDTO[]> {
    const url = `${this.baseUrl}/products/${productId}`;
    return this.http.get<CpeReturnDTO[]>(url);
  }

  public populateDictionary(): Observable<void> {
    return this.http.post<void>(this.baseUrl, {});
  }

  public assignCpeToProduct(cpeId: string, productId: string): Observable<InventoryReturnDTO> {
    const url = `${this.baseUrl}/${cpeId}/products/${productId}`;
    return this.http.post<InventoryReturnDTO>(url, {});
  }

  public getAllCPEsCount(): Observable<number> {
    return this.http.get<number>(`${this.baseUrl}/count`);
  }

  public getAllCpeNamesCount(): Observable<number> {
    return this.http.get<number>(`${this.baseUrl}/names/count`);
  }

  public getAllCpeVendorsCount(): Observable<number> {
    return this.http.get<number>(`${this.baseUrl}/vendors/count`);
  }

}
