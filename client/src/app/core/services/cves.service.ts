import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InventoryReturnDTO } from 'src/app/models/inventory/product-return.dto';
import { CveReturnDTO } from 'src/app/models/cves/cve-return.dto';
import { MatchedUriDTO } from 'src/app/models/cves/cve-matchedUri.dto';

@Injectable({
  providedIn: 'root',
})
export class CvesService {
  public baseUrl = `http://localhost:3000/cves`;

  public constructor(private readonly http: HttpClient) {}

  // not implemented
  public getAllCves(): Observable<CveReturnDTO[]> {
    return this.http.get<CveReturnDTO[]>(this.baseUrl);
  }

  public getSpecificCveById(cveId: string): Observable<CveReturnDTO> {
    const url = `${this.baseUrl}/${cveId}`;
    return this.http.get<CveReturnDTO>(url);
  }
  public getCvesForCpe(cpeId: string): Observable<CveReturnDTO[]> {
    const url = `${this.baseUrl}/cpes/${cpeId}`;
    return this.http.get<CveReturnDTO[]>(url);
  }

  public populateCVEsForCPE(
    matchedUri: MatchedUriDTO
  ): Observable<CveReturnDTO[]> {
    return this.http.post<CveReturnDTO[]>(this.baseUrl, matchedUri);
  }

  public assignCveToProduct(
    cveId: string,
    productId: string
  ): Observable<InventoryReturnDTO> {
    const url = `${this.baseUrl}/${cveId}/products/${productId}`;
    return this.http.post<InventoryReturnDTO>(url, {});
  }

  public unassignCveFromProduct(
    cveId: string,
    productId: string
  ): Observable<CveReturnDTO> {
    const url = `${this.baseUrl}/${cveId}/product/${productId}`;
    return this.http.delete<CveReturnDTO>(url);
  }
}
