import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { NotificatorService } from '../core/services/notificator.service';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../core/services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthenticationService,
    private readonly notificatorService: NotificatorService,
    private readonly router: Router
  ) {}

  canActivate(): Observable<boolean> {
    return this.authService.isLoggedIn$.pipe(
      tap((loggedIn) => {
        if (!loggedIn) {
          this.notificatorService.warn(`You are not authorized to access this page!`);
          this.router.navigate(['/login']);
        }
      })
    );
  }
}
