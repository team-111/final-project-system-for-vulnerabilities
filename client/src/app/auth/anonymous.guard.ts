import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthenticationService } from '../core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AnonymousGuard implements CanActivate {
  constructor(
    private readonly authService: AuthenticationService,
    private readonly router: Router,
  ) { }

  canActivate(): Observable<boolean> {

    return this.authService.isLoggedIn$
      .pipe(
        tap(loggedIn => {
          if (loggedIn) {
            this.router.navigate(['/dashboard']);
          }
        }),
        map(loggedIn => { if (!loggedIn) { return !loggedIn; } }),
      );
  }
}
