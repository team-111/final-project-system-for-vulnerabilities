import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { StorageService } from '../core/services/storage.service';

@Injectable()
export class TokenInterseptorService implements HttpInterceptor {
  constructor(private readonly storage: StorageService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    const token = this.storage.read('token') || '';
    const updatedRequest = request.clone({
      headers: request.headers.set('Authorization', `Bearer ${token}`),
    });

    return next.handle(updatedRequest);
  }
}
