import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { NotificatorService } from '../core/services/notificator.service';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../core/services/auth.service';
import { UserReturnDTO } from '../models/users/user-return.dto';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(
    private readonly authService: AuthenticationService,
    private readonly notificatorService: NotificatorService,
    private readonly router: Router
  ) { }

  canActivate(): Observable<boolean> {
    return this.authService.loggedUser$
      .pipe(
        map((user: UserReturnDTO) => {
          if (user.userRole !== 'Admin') {
            this.notificatorService.warn(`You are not authorized to go on that page`);
            this.router.navigate(['/dashboard']);
            return false;
          } else {
            return true;
          }
        })
      );
  }
}
