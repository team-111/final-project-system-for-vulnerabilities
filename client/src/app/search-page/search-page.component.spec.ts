import { of } from 'rxjs';
import { InventoryService } from './../inventory/services/inventory.service';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { SearchPageComponent } from './search-page.component';
import { Routes, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SearchPageRoutingModule } from './search-page-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ChangeDetectorRef } from '@angular/core';
import { InventoryReturnDTO } from '../models/inventory/product-return.dto';

describe('SearchPageComponent', () => {
  let router;
  let detectorRef;
  let inventoryService;

  let fixture: ComponentFixture<SearchPageComponent>;
  let component: SearchPageComponent;

  router = {
    navigate() { },
  };

  detectorRef = {
    detectChanges() { },
  };

  inventoryService = {
    getProductsByCpeTtitle() { },
    getProducsByCveId() { },
    getProductByNameOrVendor() { },
  };

  const routes: Routes = [
    { path: '', component: SearchPageComponent, pathMatch: 'full' },
  ];

  beforeEach(async(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        SharedModule,
        SearchPageRoutingModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [SearchPageComponent],
      providers: [
        InventoryService,
        ChangeDetectorRef,
        { provide: Router, useValue: router },
      ],
    })
      .overrideProvider(InventoryService, { useValue: inventoryService })
      .overrideProvider(ChangeDetectorRef, { useValue: detectorRef });

    fixture = TestBed.createComponent(SearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.inject(Router);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  describe('getProductByQuery()', () => {
    it('should return true with called with None', () => {
      // Arrange
      component.selected = 'None';

      // Act
      const result = component.getProductByQuery();

      // Assert
      expect(result).toBe(true);
    });

    describe('getProductsByCpeTtitle()', () => {
      it('should be called once with correct data', () => {
        // Arrange
        component.selected = 'title';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProductsByCpeTtitle')
          .mockImplementation(() => of([new InventoryReturnDTO()]));

        // Act
        component.getProductByQuery();

        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test');
      });

      it('should navigate to /search-page', () => {
        // Arrange
        component.selected = 'title';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProductsByCpeTtitle')
          .mockImplementation(() => of([new InventoryReturnDTO()]));
        const spyRoute = jest.spyOn(router, 'navigate');

        // Act
        component.getProductByQuery();

        // Assert
        expect(spyRoute).toHaveBeenCalledTimes(1);
        expect(router.navigate).toHaveBeenCalledWith(['/search-page'], {
          queryParams: { title: 'test' },
        });
      });
    });

    describe('getProducsByCveId()', () => {
      it('should be called once with correct data', () => {
        // Arrange
        component.selected = 'cveId';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProducsByCveId')
          .mockImplementation(() => of([new InventoryReturnDTO()]));

        // Act
        component.getProductByQuery();

        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test');
      });

      it('should navigate to /search-page', () => {
        // Arrange
        component.selected = 'cveId';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProducsByCveId')
          .mockImplementation(() => of([new InventoryReturnDTO()]));
        const spyRoute = jest.spyOn(router, 'navigate');

        // Act
        component.getProductByQuery();

        // Assert
        expect(spyRoute).toHaveBeenCalledTimes(1);
        expect(router.navigate).toHaveBeenCalledWith(['/search-page'], {
          queryParams: { cveId: 'test' },
        });
      });
    });

    describe('getProductByNameOrVendor()', () => {
      it('should be called once with correct data if selected value is productName', () => {
        // Arrange
        component.selected = 'productName';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProductByNameOrVendor')
          .mockImplementation(() => of([new InventoryReturnDTO()]));

        // Act
        component.getProductByQuery();

        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('productName', 'test');
      });

      it('should navigate to /search-page', () => {
        // Arrange
        component.selected = 'productName';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProductByNameOrVendor')
          .mockImplementation(() => of([new InventoryReturnDTO()]));
        const spyRoute = jest.spyOn(router, 'navigate');

        // Act
        component.getProductByQuery();

        // Assert
        expect(spyRoute).toHaveBeenCalledTimes(1);
        expect(router.navigate).toHaveBeenCalledWith(['/search-page'], {
          queryParams: { productName: 'test' },
        });
      });

      it('should be called once with correct data if selected value is productVendor', () => {
        // Arrange
        component.selected = 'productVendor';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProductByNameOrVendor')
          .mockImplementation(() => of([new InventoryReturnDTO()]));

        // Act
        component.getProductByQuery();

        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('productVendor', 'test');
      });

      it('should navigate to /search-page', () => {
        // Arrange
        component.selected = 'productVendor';
        component.valueToSearch = 'test';
        const spy = jest
          .spyOn(inventoryService, 'getProductByNameOrVendor')
          .mockImplementation(() => of([new InventoryReturnDTO()]));
        const spyRoute = jest.spyOn(router, 'navigate');

        // Act
        component.getProductByQuery();

        // Assert
        expect(spyRoute).toHaveBeenCalledTimes(1);
        expect(router.navigate).toHaveBeenCalledWith(['/search-page'], {
          queryParams: { productVendor: 'test' },
        });
      });
    });
  });
});
