import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { InventoryService } from '../inventory/services/inventory.service';
import { Router } from '@angular/router';
import { InventoryReturnDTO } from '../models/inventory/product-return.dto';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css'],
})
export class SearchPageComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  obs: Observable<any>;
  dataSource: MatTableDataSource<InventoryReturnDTO>;
  selected = 'None';
  valueToSearch: string;
  public productsLength = [];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private readonly inventoryService: InventoryService,
    public routes: Router
  ) {}
  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case 'Enter':
        this.getProductByQuery();
        break;
    }
  }

  ngOnInit() {
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  public getProductByQuery(): any {
    if (this.selected === 'None') {
      return true;
    } else {
      if (this.selected === 'title') {
        this.inventoryService
          .getProductsByCpeTtitle(this.valueToSearch)
          .subscribe((data) => {
            this.routes.navigate(['/search-page'], {
              queryParams: { title: this.valueToSearch },
            });
            this.dataSource = new MatTableDataSource<InventoryReturnDTO>(data);
            this.dataSource.paginator = this.paginator;
            this.obs = this.dataSource.connect();

            this.productsLength = data;
            this.valueToSearch = '';
          });
      } else if (this.selected === 'cveId') {
        this.inventoryService
          .getProducsByCveId(this.valueToSearch)
          .subscribe((data) => {
            this.routes.navigate(['/search-page'], {
              queryParams: { cveId: this.valueToSearch },
            });
            this.dataSource = new MatTableDataSource<InventoryReturnDTO>(data);
            this.dataSource.paginator = this.paginator;
            this.obs = this.dataSource.connect();

            this.productsLength = data;
            this.valueToSearch = '';
          });
      } else {
        this.inventoryService
          .getProductByNameOrVendor(this.selected, this.valueToSearch)
          .subscribe((data) => {
            if (this.selected === 'productName') {
              this.routes.navigate(['/search-page'], {
                queryParams: { productName: this.valueToSearch },
              });
              this.dataSource = new MatTableDataSource<InventoryReturnDTO>(
                data
              );
              this.dataSource.paginator = this.paginator;
              this.obs = this.dataSource.connect();

              this.productsLength = data;
            } else if (this.selected === 'productVendor') {
              this.routes.navigate(['/search-page'], {
                queryParams: { productVendor: this.valueToSearch },
              });
              this.dataSource = new MatTableDataSource<InventoryReturnDTO>(
                data
              );
              this.dataSource.paginator = this.paginator;
              this.obs = this.dataSource.connect();

              this.productsLength = data;
            } else {
              this.routes.navigate(['/search-page']);
            }
            this.valueToSearch = '';
          });
      }
    }
  }
}
