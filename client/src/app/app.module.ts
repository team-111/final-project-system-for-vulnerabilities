import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { TokenInterseptorService } from './auth/token-interseptor.service';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { RegisterComponent } from './components/register/register.component';
import { InventoryModule } from './inventory/inventory.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SearchPageModule } from './search-page/search-page.module';
import { SearchSingleCveModule } from './search-single-cve/search-single-cve.module';
import { UsersModule } from './users/users.module';
import { AllProductsResolverService } from './inventory/resolvers/all-products.resolver';
import { ConfirmModalComponent } from './shared/components/confirm-modal/confirm-modal.component';
import { SpinnerInterceptor } from './core/interceptors/spinner.interceptor';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    ServerErrorComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    NgxSpinnerModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),

    DashboardModule,
    SearchPageModule,
    SearchSingleCveModule,
    InventoryModule,
    UsersModule,
  ],
  providers: [
    AllProductsResolverService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterseptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
  ],
  entryComponents: [ConfirmModalComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
