import { Component, ViewChild, OnInit } from '@angular/core';
import { ChartComponent } from 'ng-apexcharts';

import { ChartType, ChartOptions } from 'chart.js';
import {
  Label,
  monkeyPatchChartJsLegend,
  monkeyPatchChartJsTooltip,
} from 'ng2-charts';

import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexXAxis,
  ApexPlotOptions,
} from 'ng-apexcharts';
import { ActivatedRoute } from '@angular/router';
import { delay } from 'rxjs/operators';

export type ChartOptions2 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
};

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('chart') chart: ChartComponent;
  public pieChartOptions: ChartOptions = {
    responsive: true,
    animation: {
      duration: 1000
    }
  };

  public pieChartLabels: Label[] = [
    ['All', 'Products'],
    ['All', 'Products with CVEs'],
    ['All', 'Products without CVEs'],
  ];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  public pieChartLabelsCPEs: Label[] = [
    ['All', 'Products'],
    ['All', 'Products with CPEs'],
    ['All', 'Products without CPEs'],
  ];
  public pieChartDataCPEs: number[] = [];
  public pieChartTypeCPEs: ChartType = 'pie';
  public pieChartLegendCPEs = true;
  public pieChartPluginsCPEs = [];

  public chartOptions2: Partial<ChartOptions2>;
  public pieColors = [
    {
      backgroundColor: ['dodgerblue', '#F79E02', 'crimson'],
    },
  ];

  constructor(private readonly activatedRoute: ActivatedRoute) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }
  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data) => {
      this.pieChartData = [
        data.allProductsCount,
        data.allWithCVEsCount,
        data.allWithoutCVEsCount,
      ];

      this.pieChartDataCPEs = [
        data.allProductsCount,
        data.allWithCPEsCount,
        data.allWithoutCPEsCount,
      ];

      this.chartOptions2 = {

        series: [
          {
            name: 'basic',
            data: [
              data.allCpesCount,
              data.allCpeNamesCount,
              data.allCpeVendorsCount,
            ],
          },
        ],
        chart: {
          type: 'bar',
          height: 300,
          width: 950,
        },
        plotOptions: {
          bar: {
            horizontal: true,
          },
        },
        dataLabels: {
          enabled: false,
        },
        xaxis: {
          categories: ['All CPEs', 'All CPE Names', 'All CPE Vendors'],
        },
      };
    });
  }
}
