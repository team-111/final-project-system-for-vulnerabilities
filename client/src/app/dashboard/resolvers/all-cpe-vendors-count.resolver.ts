import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { CpesService } from 'src/app/core/services/cpes.service';

@Injectable({ providedIn: 'root' })
export class AllCpeVendorsResolverService implements Resolve<number> {
  constructor(private readonly cpeService: CpesService) {}

  resolve(acitvatedRoute: ActivatedRouteSnapshot): Observable<number> {
    const allVendorsCount = this.cpeService.getAllCpeVendorsCount();

    return allVendorsCount;
  }
}
