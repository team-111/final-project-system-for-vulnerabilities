import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { InventoryService } from 'src/app/inventory/services/inventory.service';

@Injectable({ providedIn: 'root' })
export class AllWithCPEsCountResolverService implements Resolve<number> {
  constructor(private readonly inventoryService: InventoryService) {}

  resolve(acitvatedRoute: ActivatedRouteSnapshot): Observable<number> {
    const allProductsWithCPEsCount = this.inventoryService.getAllProductsWithOrWithoutCPEsCount(
      true
    );

    return allProductsWithCPEsCount;
  }
}
