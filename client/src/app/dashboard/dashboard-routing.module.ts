import { AllCpeVendorsResolverService } from './resolvers/all-cpe-vendors-count.resolver';
import { AllCpeNamesResolverService } from './resolvers/all-cpe-names-count.resolver';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AllProductsCountResolverService } from './resolvers/all-products-count.resolver';
import { AllWithCVEsCountResolverService } from './resolvers/products-with-cves-count.resolver';
import { AllWithoutCVEsCountResolverService } from './resolvers/products-without-cves-count.resolver';
import { AllWithCPEsCountResolverService } from './resolvers/products-with-cpes-count.resolver';
import { AllWithoutCPEsCountResolverService } from './resolvers/products-without-cpes-count.resolver';
import { AllCpesCountResolverService } from './resolvers/all-cpes-count.resolver';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    pathMatch: 'full',
    resolve: {
      allProductsCount: AllProductsCountResolverService,
      allWithCVEsCount: AllWithCVEsCountResolverService,
      allWithoutCVEsCount: AllWithoutCVEsCountResolverService,
      allWithCPEsCount: AllWithCPEsCountResolverService,
      allWithoutCPEsCount: AllWithoutCPEsCountResolverService,
      allCpesCount: AllCpesCountResolverService,
      allCpeNamesCount: AllCpeNamesResolverService,
      allCpeVendorsCount: AllCpeVendorsResolverService,
    },
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
